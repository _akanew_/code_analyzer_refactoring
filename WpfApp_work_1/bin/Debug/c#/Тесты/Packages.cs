using System;

using Domain;
using DataFactory;

namespace SomeApp
{
    public class Program
    {
	public static void Main() 
	{
	}

	public void SetParams(DomainFacade dfcd, 
		DFactory df)
	{
	}
    }
}

using System;
using DataSource;

namespace Domain
{
    public class DomainFacade
    {
	Product prod;
	Customer cust;
	IDataSource ds;

	public DomainFacade(IDataSource src) {} 

	public void Some()
	{
	   Product pr;
	   Customer cust;	
	}
    }	

    public class Customer
    {
	public string FirstName {get; set;}
	public string LastName {get; set;}
    } 			
	
    public class Product
    {
	public string Name {get; set;}
	public float Price {get; set;}
    }			
}

using System;
using System.Data;

namespace DataSource
{
     public interface IDataSource
     {
	 DataTable GetData();
     }

     public class TextSource : IDataSource
     { 
	 public DataTable GetData()
	 {
   	     return new DataTable();
	 }
     }		 
}

using System;
using DataSource;

namespace DataFactory
{
     public class DFactory
     {
	 IDataSource GetSource() 
         {
             IDataSource isrc; 
	 };
     }	
} 