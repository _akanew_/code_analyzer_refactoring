﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Text.RegularExpressions; //для регулярных выражений
using WpfApp_work_1.Data;  //для передачи выбраных файлов для анализа
using System.IO; //для работы с файлом
using System.ComponentModel;// для INotifyPropertyChanged

namespace WpfApp_work_1
{
    /// <summary>
    /// Логика взаимодействия для OpenFileDialog.xaml
    /// </summary>
    /// 
   

    public partial class OpenFileDialog : Window
    {
        string[] files;
       public DataInput dataInput = new DataInput();



        public List<BoolStringFile> TheList { get; set; }
       // public BoolStringFile[] TheList2 { get; set; }
        Regex regex_typeFile = new Regex(@"\.cs$");  

        public OpenFileDialog(string[] files)   //files - список всех файлов в папке
        {
            InitializeComponent();
            this.files = files;
            this.DataContext = this;



            TheList = new List <BoolStringFile>();
         //   TheList2 = new BoolStringFile[10];

            OFD_Listbox.Items.Clear();

            foreach (string folders in files)
            {
                //OFD_Listbox.Items.Add(folders); //вариант 1 работает
                if (   regex_typeFile.IsMatch(folders)   )
                    TheList.Add(new BoolStringFile { IsSelected = true, File = folders });
                else
                    TheList.Add(new BoolStringFile { IsSelected = false, File = folders });
            }

            // OFD_Listbox.ItemsSource = files; //вариант 2  работает
           

        }

        private void Button_ok_Click(object sender, RoutedEventArgs e)
        {

            dataInput.Path_manyText = new List<string>();
            var builder = new StringBuilder();//для объединения

            string str = ("\r\n //********************************************************************************************\r\n");

            foreach (BoolStringFile xxx in OFD_Listbox.ItemsSource)
            {

                if (xxx.IsSelected == true)
                {
                    dataInput.Path_manyText.Add(xxx.File);

                    // MessageBox.Show(xxx.File);  //вывести окно с именем файла

                    //str2 = str.Insert((str.Length/2), xxx.File);

                    // builder.Append(xxx.File);//объединяем тексты в 1 файл
                    builder.Append(str.Insert((str.Length / 2), xxx.File));
                    builder.Append(File.ReadAllText(xxx.File, Encoding.Default));//объединяем тексты в 1 файл

                    //   test.Items.Add(xxx.File);
                }
            }
                File.WriteAllText(@"c#\fileOutput0.txt", builder.ToString(), Encoding.Default); //Encoding.Default важно

                // File.ReadAllText(c#\fileOutput0.txt, Encoding.Default)

                //удаляем комментарии
             //   var builder_free_com = new StringBuilder();//для объединения

                string[] text = File.ReadAllLines(@"c#\fileOutput0.txt", Encoding.Default);
              // Regex regex_del = new Regex(@"\/\/.+?\n|\/\*.*?\*\/\s*");//замена и удаление пробелов вначале строки и комментариев

                StreamWriter f = new StreamWriter(@"c#\fileOutput.txt");

                string string_del;
                for (int i = 0; i < text.Length; i++)//перебор с начала до конца текста
                {
                    // string string_del = regex_del.Replace(text[i], String.Empty);

                    string string1= text[i];
                    int index = text[i].IndexOf("//");
                    int del = text[i].Length - index;
                    if (index>=0)
                         string_del = text[i].Substring(0,index);
                    else
                        string_del = text[i];
                    //builder_free_com.Append("\n\r", string_regex_del);
                    //builder_free_com.Append(text[i]);

                    f.WriteLine(string_del);

                }
                f.Close();
               // File.WriteAllText(@"c#\fileOutput.txt", builder_free_com.ToString(), Encoding.Default); //Encoding.Default важно

                //this.Hide();
                this.Close();

           // test.ItemsSource = dataInput.Path_text;

        }

       

        private void Button_select_Click(object sender, RoutedEventArgs e)
        {

          //   TheList = new List<BoolStringFile>();
          //    OFD_Listbox.Items.Clear();
            //OFD_Listbox.UpdateLayout();
            for (int i=0; i< TheList.Count; i++)
            {

                TheList[i].IsSelected = true;
              
                    
            }
        }

        private void Button_clear_Click(object sender, RoutedEventArgs e)
        {


            for (int i = 0; i < TheList.Count; i++)
            {

                TheList[i].IsSelected = false;


            }


        }
    }


    public class BoolStringFile0
    {
        // public int Id { get; set; }
        public string File { get; set; }
        public bool IsSelected { get; set; }
    }


   

   public class BoolStringFile : INotifyPropertyChanged
    {
        string    file ;
        public string File

        {
            get { return file; }
            set { if (file != value)                { file = value; NotifyPropertyChanged(); } }
        }

        bool isSelected; 
        public bool IsSelected   
        {
            get { return isSelected; }
            set { if (isSelected != value) { isSelected = value; NotifyPropertyChanged(); } }
        }

        void NotifyPropertyChanged(string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

       
    }





    //class IntWithChoiceVM : DependencyObject
    //{
    //    public int Value
    //    {
    //        get { return (int)GetValue(ValueProperty); }
    //        set { SetValue(ValueProperty, value); }
    //    }

    //    public static readonly DependencyProperty ValueProperty =
    //        DependencyProperty.Register(
    //                "Value", typeof(int), typeof(IntWithChoiceVM));

    //    public bool IsSelected
    //    {
    //        get { return (bool)GetValue(IsSelectedProperty); }
    //        set { SetValue(IsSelectedProperty, value); }
    //    }

    //    public static readonly DependencyProperty IsSelectedProperty =
    //        DependencyProperty.Register(
    //                 "IsSelected", typeof(bool), typeof(IntWithChoiceVM));
    //}


    //class ListChoiceVM : DependencyObject
    //{
    //    public ListChoiceVM(List<int> initial)
    //    {
    //        Values = new ObservableCollection();
    //        foreach (var n in initial)
    //            Values.Add(new IntWithChoiceVM() { Value = n, IsSelected = true });
    //    }

    //    public ObservableCollection<IntWithChoiceVM> Values { get; private set; }

    //    public IEnumerable<int> GetSelectedInts()
    //    {
    //        return Values.Where(v => v.IsSelected).Select(v => v.Value);
    //    }
    //}












}
