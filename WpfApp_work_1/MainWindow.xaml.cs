﻿// *5 задача расположить элементы при создании +
// *6 при обращении вывести текст на отдельную панель в текст блок +
// *7 рисуем линию к текущему расположению мыши  +
// *8 получить координаты объекта    +                               Vector vector = VisualTreeHelper.GetOffset(draggedObject); Point vector_to_Point = new Point(vector.X, vector.Y);
// *9 при выборе объекта получить его номер +                    //  label1[i].Tag = i; //var i = draggedObject.Tag; 
// *10 рисуем линии при создании объекта
// *11  перемещение линий вместе с объектом

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WpfApp_work_1.Data;

using System.IO; //для работы с файлом
using System.Text.RegularExpressions; //для регулярных выражений

using System.Collections; //для ArrayList
using System.ComponentModel; //INotifyPropertyChanged для отрисовки: компоновки лабелов,


namespace WpfApp_work_1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window  , INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;// пока все дальше не реализовано

        //для CreateLabel()
        Label[] label1 = new Label[100];  //переделать на лист?
        double[] y_canvas = new double[100]; //переделать на лист?

        FrameworkElement draggedObject;  // объект перетаскивания
                                       
        //для линий
        double[] x = new Double[100];
        double[] y = new Double[100];

        // для добавления лейбела в методе  create_label
        Random rnd = new Random(); // test 
        int[] rundom = new int[100]; // test 

        //для линий
     //   Polyline[] lineRelation = new Polyline[100];
        bool x_area = true; //для начальной позиции рисования
        bool y_area = false;//для начальной позиции рисования

        Point Point1, Point2, Point3, Point4, PointTriangle1, PointTriangle2,PointTriangle3, PointTriangle4;

        //test for polylinetest
        Point a1 = new Point(0, 0);
        Point a2 = new Point(200, 200);
        Polyline polylinetest = new Polyline(); //вынесено в начало

        System.Windows.Shapes.Line[] myLineTestClassIndex = new System.Windows.Shapes.Line[100];

        private Point? movePoint;     //нужно для....?

        DataInput pathString = new DataInput();

        #region //Данные для класса FindClass
        List<FindClass> findClass = new List<FindClass>();

        #endregion

        #region //Данные для класса Relation
       Relation relation1 = new Relation();

        #endregion

        #region //Данные для рисования линий зависимости  по классу Relation
        Polyline[,] lineRelation = new Polyline[100,100];
        Polyline[,] lineAssociation = new Polyline[100, 100];
        Polyline[,] lineAgregation = new Polyline[100, 100];

        //для стрелок связей - треугольник
        Polyline[,] polylineArrowDependent = new Polyline[100, 100];
        Polyline[,] polylineArrowAssociation = new Polyline[100, 100];
        Polyline[,] polylineArrowAgregation = new Polyline[100, 100];
        Polyline[,] polylineRombeAssociation = new Polyline[100, 100]; //ромб у ассоциации
        Polyline[,] polylineRombeAgregation = new Polyline[100, 100]; //ромб у ассоциации

        #endregion

        TreeViewItem item = new TreeViewItem();//отображение класса тест

        //тест кривая безье  не работает
        System.Windows.Shapes.Path pathpath = new System.Windows.Shapes.Path();
        BezierSegment bezierSegment = new BezierSegment();
        PathFigure pathFigure = new PathFigure();
        PathGeometry pathGeometry = new PathGeometry();

        static DataInput datainput1 = new DataInput(); //ввод данных 1й текст
       // Analyze analyze1 = new Analyze(datainput1.path_text0, datainput1.path_field0, datainput1.path_2_ToMask0);  //создаем и анализируем 1 текст
        Analyze analyze1;//= new Analyze(SelectFolder.dataInput.path_text, SelectFolder.dataInput.path_field, SelectFolder.dataInput.path_2_ToMask);  //создаем и анализируем 1 текст

        public MainWindow()
        {
            InitializeComponent();
            DragArena.Height = 1500;// * ScaleRate;// = DragArena.Height* (ScaleRate*0.95);  //узнаем высоту канваса первоначально потом надо сделать умножение на коэффициент
            DragArena.Width = 1500;// * ScaleRate; //;= DragArena.Width * (ScaleRate*0.95);
            DragArena.UpdateLayout();
        }

        private void Btn_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
   
            //movePoint = e.GetPosition(btn);         // перемещение лабла btn
            //btn.CaptureMouse();

            draggedObject = (FrameworkElement)sender;
            movePoint = e.GetPosition(draggedObject);
            //  draggedObject.CaptureMouse(); // захват мышью
            Mouse.Capture(draggedObject); // захват вариант 2

            //ширина и высота в лабел
            #region // *6 при обращении вывести текст на отдельную панель в текст блок   + получение координатов
            //выводит текст лабела в текст блок справа
            // label_to_box.Text = inf1.Content;
            //label_to_box.MaxLength = 1;// не нужно длина ввода

            //label_to_box.Text = inf1.Content.ToString() + Environment.NewLine;
            Point position = Mouse.GetPosition(DragArena); // позиция мышки относительно канваса DragArena


            Point point_screen = new Point();
            //Point pointnow2 = new Point();
            point_screen = draggedObject.PointToScreen(new Point(draggedObject.ActualWidth, draggedObject.ActualHeight));
            //pointnow2 = draggedObject.PointToScreen(new Point(draggedObject.ActualWidth / 2, draggedObject.ActualHeight / 2));
          //  label_to_box.Text += "position=" + position + Environment.NewLine + "point_screen=" + point_screen + Environment.NewLine; //draggedObject.PointToScreen(new Point(draggedObject.ActualWidth, draggedObject.ActualHeight));//получаем координаты центра объекта //*************сохранить*********************.PointToScreen(new Point(button.ActualWidth, button.ActualHeight)); //получить координаты центра button

            #endregion


            // *7 рисуем линию  позицию нажатия  работает

            #region// *8 получить координаты объекта 
            Vector vector = VisualTreeHelper.GetOffset(draggedObject);
            // Convert the vector to a point value.
            Point vector_to_Point = new Point(vector.X, vector.Y);
            #endregion

            #region// *9 при выборе объекта получить его номер 
            var tag = draggedObject.Tag;
            int iTag = Convert.ToInt32(tag);
           // label_to_box.Text += "элемент taq n=" + iTag;
           // label_to_box.Text += Environment.NewLine + "x[i] =" + x[iTag] + " y[i] =" + y[iTag] + " rundom[i]=" + rundom[iTag];
            #endregion
           
            FrameworkElement draggedObject1, draggedObject2;
            draggedObject2 = (FrameworkElement)sender;

            tag = draggedObject2.Tag;
            iTag = Convert.ToInt32(tag); //номер линии

            draggedObject1 = (label1[0]); //объект 1 предыдущий ( по идее который выше)              //переходим на рисование таблицы

            double Width1 = draggedObject1.ActualWidth; //ширина предыдущего объекта (объект 1)
            double Width2 = draggedObject2.ActualWidth;//ширина текущего объекта (объект 2)

            Vector vector1 = VisualTreeHelper.GetOffset(draggedObject1);
            // Convert the vector to a point value.
            Point Point00_Object1 = new Point(vector1.X, vector1.Y); //координаты начала объекта 1
            double H1 = Point00_Object1.X; //начало x объекта 1
            double K1 = H1 + Width1; //конец х объекта 1

            Vector vector2 = VisualTreeHelper.GetOffset(draggedObject2);
            // Convert the vector to a point value.
            Point Point00_Object2 = new Point(vector2.X, vector2.Y); //координаты начала объекта 2
            double H2 = Point00_Object2.X; //начало x объекта 1
            double K2 = H2 + Width2;

            double C1 = Width1 / 2 + H1; //середина объекта 1
            double C2 = Width2 / 2 + H2; ; //середина объекта 2

            #region //выводим информацию из лабелов
             tag = draggedObject.Tag;
             iTag = Convert.ToInt32(tag);
            #endregion
        }

        private void Btn_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            draggedObject = (FrameworkElement)sender;
            movePoint = null;
            draggedObject.ReleaseMouseCapture();
        }

        #region  // методы для нарисованных фигур

        Vector relativeMousePos;

        void StartDrag(object sender, MouseEventArgs e) // изменяем событие. было (object sender, MouseButtonEventArgs e)
        {
            draggedObject = (FrameworkElement)sender;
            relativeMousePos = e.GetPosition(draggedObject) - new Point();
            draggedObject.MouseMove += OnDragMove;
            draggedObject.LostMouseCapture += OnLostCapture;
            draggedObject.MouseUp += OnMouseUp;
            Mouse.Capture(draggedObject);
        }

        void OnDragMove(object sender, MouseEventArgs e)
        {
            UpdatePosition(e);
            Point position_mouse = Mouse.GetPosition(DragArena); // позиция мышки относительно канваса DragArena

            #region// *11  рисование 

            FrameworkElement draggedObjectNext, draggedObjectMouse;
            draggedObjectMouse = (FrameworkElement)sender;//  выделеный мышкой объект
 
            int tagMouse = Convert.ToInt32(draggedObjectMouse.Tag); //номер объекта который выделен

            //определим номера объектов для перерисовки связей
            draggedObjectNext = (label1[0]); //объект 1   //временно тест если выбранный элеент не "первый"
            int tagNext = Convert.ToInt32(draggedObjectNext.Tag); //номер объекта который выделен

            List<int> listRelation = new List<int>();
            bool l_reverce = false;

            for (int i = 0; i < relation1.Table.Count; i++)
                if (relation1.Table[i][0] == tagMouse)                 //если первый элемент в таблице связей равен выбранному объекту мышкой
                {
                    listRelation = relation1.Table[i];             // то рисуем зависимость по этой таблице relation1.Table  
                }
            Draw_line_universal(lineRelation, polylineArrowDependent, listRelation, draggedObjectMouse, l_reverce);

            l_reverce = true;  //  выбранный элемент не равен первому эм в таблице relation1.Table то рисуем  по relation1.TableReverce
            listRelation = new List<int>();
            for (int i = 0; i < relation1.TableReverce.Count; i++)
                if (relation1.TableReverce[i][0] == tagMouse)
                {
                    listRelation = relation1.TableReverce[i];   //
                }

             Draw_line_universal(lineRelation, polylineArrowDependent, listRelation, draggedObjectMouse, l_reverce);

            ///////////////////Перерисовка ассоциаций
            List<int> listRelationAssociation = new List<int>();
            l_reverce = false;
            for (int i = 0; i < relation1.TableComposition.Count; i++)
                if (relation1.TableComposition[i][0] == tagMouse)                 //если первый элемент в таблице связей равен выбранному объекту мышкой
                {
                    listRelationAssociation = relation1.TableComposition[i];             // то рисуем зависимость по этой таблице
                }

            Draw_line_universal(lineAssociation, polylineArrowAssociation, listRelationAssociation, draggedObjectMouse, l_reverce);

            l_reverce = true;
            Draw_line_rombe(polylineRombeAssociation, listRelationAssociation, draggedObjectMouse, l_reverce);

            l_reverce = true;
            for (int i = 0; i < relation1.TableCompositionReverce.Count; i++)
                if (relation1.TableCompositionReverce[i][0] == tagMouse)                 //если первый элемент в таблице связей равен выбранному объекту мышкой
                {
                    listRelationAssociation = relation1.TableCompositionReverce[i];             // то рисуем зависимость по этой таблице 
                }
            Draw_line_universal(lineAssociation, polylineArrowAssociation, listRelationAssociation, draggedObjectMouse, l_reverce);

            l_reverce = false;
            Draw_line_rombe(polylineRombeAssociation, listRelationAssociation, draggedObjectMouse, l_reverce);

            #endregion

            #region //перерисовка агрегации

            ///////////////////Перерисовка ассоциаций
            List<int> listRelationAgregation = new List<int>();
            l_reverce = false;
            for (int i = 0; i < relation1.TableAgregation.Count; i++)
                if (relation1.TableAgregation[i][0] == tagMouse)                 //если первый элемент в таблице связей равен выбранному объекту мышкой
                {
                    listRelationAgregation = relation1.TableAgregation[i];             // то рисуем зависимость по этой таблице
                }

            // Draw_line(listRelation, draggedObjectMouse, l_reverce);

            // Draw_line_universal(lineAssociation,listRelation, draggedObjectMouse, l_reverce);
            // Draw_lineAssociation(listRelation, draggedObjectMouse, l_reverce);
            Draw_line_universal(lineAgregation, polylineArrowAgregation, listRelationAgregation, draggedObjectMouse, l_reverce);

            l_reverce = true;
            Draw_line_rombe(polylineRombeAgregation, listRelationAgregation, draggedObjectMouse, l_reverce);

            l_reverce = true;
            for (int i = 0; i < relation1.TableAgregationReverce.Count; i++)
                if (relation1.TableAgregationReverce[i][0] == tagMouse)                 //если первый элемент в таблице связей равен выбранному объекту мышкой
                {
                    listRelationAgregation = relation1.TableAgregationReverce[i];             // то рисуем зависимость по этой таблице
                }
            Draw_line_universal(lineAgregation, polylineArrowAgregation, listRelationAgregation, draggedObjectMouse, l_reverce);

            l_reverce = false;
            Draw_line_rombe(polylineRombeAgregation, listRelationAgregation, draggedObjectMouse, l_reverce);

            #endregion
        }

        //void  Draw_line()

        void Draw_line(Polyline[,] line, Polyline[,] arrow,  
            List<int> list, FrameworkElement draggedObjectMouse, bool l_reverce, bool romb) //, int tagMouse)
        {
            for (int j = 1; j < list.Count; j++) 
            {
                int tagMouse = Convert.ToInt32(draggedObjectMouse.Tag); //номер объекта взятого мышкой

                FrameworkElement draggedObjectNext = (label1[list[j]]); //объект 1 . идет перебор связей
                int tagNext = Convert.ToInt32(draggedObjectNext.Tag); //номер объекта следующего по зависимости не взятого мышкой

                int start = tagMouse;
                int stop = tagNext;

                if (l_reverce == true)
                {
                    start = tagNext;
                    stop = tagMouse;
                }

                x_area = true; //для начальной позиции рисования
                y_area = false;//для начальной позиции рисования

                //получение координат 
                Vector vector1 = VisualTreeHelper.GetOffset(draggedObjectNext);
                Vector vector2 = VisualTreeHelper.GetOffset(draggedObjectMouse);
                // Convert the vector to a point value.
                Point Point00_Object1 = new Point(vector1.X, vector1.Y); //координаты начала объекта 1
                Point Point00_Object2 = new Point(vector2.X, vector2.Y); //координаты начала объекта 2
                //вычисление координат объекта 1
                double width1 = draggedObjectNext.ActualWidth; //ширина предыдущего объекта (объект 1) x
                double height1 = draggedObjectNext.ActualHeight; //высота пред объекта 1 y
                double left1 = Point00_Object1.X; //начало объекта 1 x
                double right1 = left1 + width1; //конец объекта 1 x
                double up1 = Point00_Object1.Y; //верх объекта 1 y
                double down1 = up1 + height1; //низ объекта 1 y
                double C1X = width1 / 2 + left1;// середина  1 х
                double C1Y = height1 / 2 + up1;// середина  1 y

                //вычисление координат объекта 2
                double width2 = draggedObjectMouse.ActualWidth;//ширина текущего объекта (объект 2) x
                double height2 = draggedObjectMouse.ActualHeight; //высота пред объекта 2 y
                double left2 = Point00_Object2.X; //начало объекта 2 x
                double right2 = left2 + width2; //конец объекта 2 x
                double up2 = Point00_Object2.Y; //верх объекта 2 y
                double down2 = up2 + height2; //низ объекта 2 y
                double C2X = width2 / 2 + left2; // середина  2 х
                double C2Y = height2 / 2 + up2; // середина  2 y

                double H1X = left1; //начало 1 х
                double K1X = right1; // конец 1 х
                double H1Y = down1;// начало 1 у
                double K1Y = up1;// конец 1 у

                double H2X = left2; //начало 2 х
                double K2X = right2; // конец 2 х
                double H2Y = down2;// начало 2 у
                double K2Y = up2;// конец 2 у

                double X, Y;
                double X1 = 0; //просто объявление выдает ошибку
                double X2 = 0;//просто объявление выдает ошибку
                double Y1 = 0; //просто объявление выдает ошибку
                double Y2 = 0;//просто объявление выдает ошибку
                double maxX = 0;//просто объявление выдает ошибку
                double minX = 0;//просто объявление выдает ошибку
                double maxY = 0;//просто объявление выдает ошибку
                double minY = 0;//просто объявление выдает ошибку
                double YTriangle = 0;
                double XTriangle = 0;

                //шаг1 определение области попадания горизонталь или вертикаль
                //рисование в области х (объект 2 ниже или выше 1)

                if (x_area == true)  //попали в горизонталь
                {
                    if (up2 > down1) // 2 ниже 1
                    {
                        Y1 = down1;
                        Y2 = up2;
                        maxY = Y2;
                        minY = Y1;
                        YTriangle = -10;

                    }
                    else if (down2 < up1) //2 выше 1
                    {
                        Y1 = up1;
                        Y2 = down2;
                        maxY = Y1;
                        minY = Y2;
                        YTriangle = 10;
                    }

                    if ((H1X <= C2X && C2X <= K1X) && (H2X <= C1X && C1X <= K2X))  //середины находятся в пределах фигур по х 
                    {
                        if (C2X <= C1X)//2 слева от 1
                        {
                            maxX = C1X;
                            minX = C2X;
                        }
                        else   // 2 справа от 1
                        {
                            maxX = C2X;
                            minX = C1X;
                        }

                        X = (maxX - minX) / 2 + minX;

                        //РИСУЕМ ПРЯМУЮ ОТ 2 К 1
                        Point1 = new Point(X, Y1);
                        Point2 = new Point(X, Y2);
                        Point3 = Point2;
                        Point4 = Point2;

                        if (!romb)
                            Update_line_universal(line, start, stop, Point1, Point2, Point3, Point4);

                        //для стрелки           
                        if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
                        {
                            PointTriangle1 = new Point(X - 10, Y2 + YTriangle);
                            PointTriangle2 = Point2;
                            PointTriangle3 = new Point(X + 10, Y2 + YTriangle);
                            PointTriangle4 = new Point(Point2.X, Point2.Y + YTriangle*2);  //для ромба ассоциации
                        }
                        else
                        {
                            PointTriangle1 = new Point(X - 10, Y1 - YTriangle);
                            PointTriangle2 = Point1;
                            PointTriangle3 = new Point(X + 10, Y1 - YTriangle);
                            PointTriangle4 = new Point(Point1.X, Point1.Y - YTriangle*2);  //для ромба ассоциации
                        }
                        if (!romb)
                            Update_Arrow_universal(arrow, start, stop, PointTriangle1, PointTriangle2, PointTriangle3);
                        else
                            Update_Rombe(line, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
                    }
                    else   //середины находятся за пределами фигур по х
                    {
                        //for polyline
                        Y = ((maxY - minY) / 2) + minY;

                        if (Y <= 0) //определение области рисования для линии (гориpонтальная = true)
                        {
                            x_area = false;
                            y_area = true;
                        }
                        //РИСУЕМ СТУПЕНЬ ОТ 2 к 1  или 1 к 2?
                        Point1 = new Point(C1X, Y1);
                        Point2 = new Point(C1X, Y);
                        Point3 = new Point(C2X, Y);
                        Point4 = new Point(C2X, Y2);

                        if (!romb)
                           Update_line_universal(line, start, stop, Point1, Point2, Point3, Point4);

                        //для стрелки                        

                        if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
                        {
                            PointTriangle1 = new Point(C2X - 10, Y2 + YTriangle);
                            PointTriangle2 = new Point(C2X, Y2);
                            PointTriangle3 = new Point(C2X + 10, Y2 + YTriangle);
                        }
                        else
                        {
                            PointTriangle1 = new Point(C1X - 10, Y1 - YTriangle);
                            PointTriangle2 = new Point(C1X, Y1);
                            PointTriangle3 = new Point(C1X + 10, Y1 - YTriangle);
                        }
                        if (!romb)
                            Update_Arrow_universal(arrow, start, stop, PointTriangle1, PointTriangle2, PointTriangle3);
                        else
                            Update_Rombe(line, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
                    }
                }

                //рисование в области y(объект 2справа или слева 1)
                if (y_area == true)
                {
                    if (left2 > right1) // 2 справа от 1
                    {
                        X1 = right1;
                        X2 = left2;
                        maxX = X2;
                        minX = X1;
                        XTriangle = -10;
                    }
                    else if (right2 < left1) //2 слева от 1
                    {
                        X1 = left1;
                        X2 = right2;
                        maxX = X1;
                        minX = X2;
                        XTriangle = 10;
                    }

                    if ((H1Y >= C2Y && C2Y >= K1Y) && (H2Y >= C1Y && C1Y >= K2Y))  //середины находятся в пределах фигур по Y
                    {
                        if (C2Y >= C1Y) //середина 2 ниже середины 1
                        {
                            maxY = C2Y;
                            minY = C1Y;
                        }
                        else
                        {
                            maxY = C1Y;
                            minY = C2Y;
                        }

                        Y = ((maxY - minY) / 2) + minY;

                        //РИСУЕМ ПРЯМУЮ горизонтальную ОТ 2 к 1
                        //for polyline
                        Point1 = new Point(X1, Y);
                        Point2 = new Point(X2, Y);
                        Point3 = Point2;
                        Point4 = Point2;

                        if (!romb)
                            Update_line_universal(line, start, stop, Point1, Point2, Point3, Point4);

                        //для стрелки                       

                        if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
                        {
                            PointTriangle1 = new Point(X2 + XTriangle, Y + 10);
                            PointTriangle2 = Point2;
                            PointTriangle3 = new Point(X2 + XTriangle, Y - 10);
                        }
                        else
                        {
                            PointTriangle1 = new Point(X1 - XTriangle, Y + 10);
                            PointTriangle2 = Point1;
                            PointTriangle3 = new Point(X1 - XTriangle, Y - 10);
                        }

                        if (!romb)
                            Update_Arrow_universal(arrow, start, stop, PointTriangle1, PointTriangle2, PointTriangle3);
                        else
                            Update_Rombe(line, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
                    }
                    else
                    {
                        //for polyline
                        X = (maxX - minX) / 2 + minX;

                        if (X <= 0)  //определение области рисования для линии (вертикальная = true)
                        {
                            x_area = true;
                            y_area = false;
                        }

                        //РИСУЕМ СТУПЕНЬ горизонтальную ОТ 2 к 1
                        Point1 = new Point(X1, C1Y);
                        Point2 = new Point(X, C1Y);
                        Point3 = new Point(X, C2Y);
                        Point4 = new Point(X2, C2Y);

                        if (!romb)
                            Update_line_universal(line, start, stop, Point1, Point2, Point3, Point4);

                        //для стрелки                       
                        if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
                        {
                            PointTriangle1 = new Point(X2 + XTriangle, C2Y + 10);
                            PointTriangle2 = new Point(X2, C2Y);
                            PointTriangle3 = new Point(X2 + XTriangle, C2Y - 10);
                        }
                        else
                        {
                            PointTriangle1 = new Point(X1 - XTriangle, C1Y + 10);
                            PointTriangle2 = new Point(X1, C1Y);
                            PointTriangle3 = new Point(X1 - XTriangle, C1Y - 10);
                        }
                        if (!romb)
                            Update_Arrow_universal(arrow, start, stop, PointTriangle1, PointTriangle2, PointTriangle3);
                        else
                            Update_Rombe(line, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
                    }
                }
            }
        }

        void Draw_line_universal(Polyline[,] line, Polyline[,] arrow, 
            List<int> list, FrameworkElement draggedObjectMouse, bool l_reverce)
        {
            Draw_line(line, arrow, list, draggedObjectMouse, l_reverce, false);
        }

        void Draw_line_rombe(Polyline[,] rombe, List<int> list, FrameworkElement draggedObjectMouse, bool l_reverce)
        {
            Draw_line(rombe, null, list, draggedObjectMouse, l_reverce, true);
        }

        //void Draw_line_rombe( Polyline[,] rombe, List<int> list, FrameworkElement draggedObjectMouse, bool l_reverce) //, int tagMouse)
        //{

        //    for (int j = 1; j < list.Count; j++)
        //    {
        //        int tagMouse = Convert.ToInt32(draggedObjectMouse.Tag); //номер объекта взятого мышкой

        //        FrameworkElement draggedObjectNext = (label1[list[j]]); //объект 1 . идет перебор связей
        //        int tagNext = Convert.ToInt32(draggedObjectNext.Tag); //номер объекта следующего по зависимости не взятого мышкой

        //        int start = tagMouse;
        //        int stop = tagNext;

        //        //test
        //        if (l_reverce == true)
        //        {
        //            start = tagNext;
        //            stop = tagMouse;

        //        }

        //        x_area = true; //для начальной позиции рисования
        //        y_area = false;//для начальной позиции рисования

        //        //получение координат 
        //        Vector vector1 = VisualTreeHelper.GetOffset(draggedObjectNext);
        //        Vector vector2 = VisualTreeHelper.GetOffset(draggedObjectMouse);
        //        // Convert the vector to a point value.
        //        Point Point00_Object1 = new Point(vector1.X, vector1.Y); //координаты начала объекта 1
        //        Point Point00_Object2 = new Point(vector2.X, vector2.Y); //координаты начала объекта 2

        //        //вычисление координат объекта 1
        //        double width1 = draggedObjectNext.ActualWidth; //ширина предыдущего объекта (объект 1) x
        //        double height1 = draggedObjectNext.ActualHeight; //высота пред объекта 1 y
        //        double left1 = Point00_Object1.X; //начало объекта 1 x
        //        double right1 = left1 + width1; //конец объекта 1 x
        //        double up1 = Point00_Object1.Y; //верх объекта 1 y
        //        double down1 = up1 + height1; //низ объекта 1 y
        //        double C1X = width1 / 2 + left1;// середина  1 х
        //        double C1Y = height1 / 2 + up1;// середина  1 y

        //        //вычисление координат объекта 2
        //        double width2 = draggedObjectMouse.ActualWidth;//ширина текущего объекта (объект 2) x
        //        double height2 = draggedObjectMouse.ActualHeight; //высота пред объекта 2 y
        //        double left2 = Point00_Object2.X; //начало объекта 2 x
        //        double right2 = left2 + width2; //конец объекта 2 x
        //        double up2 = Point00_Object2.Y; //верх объекта 2 y
        //        double down2 = up2 + height2; //низ объекта 2 y
        //        double C2X = width2 / 2 + left2; // середина  2 х
        //        double C2Y = height2 / 2 + up2; // середина  2 y

        //        double H1X = left1; //начало 1 х
        //        double K1X = right1; // конец 1 х
        //        double H1Y = down1;// начало 1 у
        //        double K1Y = up1;// конец 1 у

        //        double H2X = left2; //начало 2 х
        //        double K2X = right2; // конец 2 х
        //        double H2Y = down2;// начало 2 у
        //        double K2Y = up2;// конец 2 у

        //        double X, Y;
        //        double X1 = 0; //просто объявление выдает ошибку
        //        double X2 = 0;//просто объявление выдает ошибку
        //        double Y1 = 0; //просто объявление выдает ошибку
        //        double Y2 = 0;//просто объявление выдает ошибку
        //        double maxX = 0;//просто объявление выдает ошибку
        //        double minX = 0;//просто объявление выдает ошибку
        //        double maxY = 0;//просто объявление выдает ошибку
        //        double minY = 0;//просто объявление выдает ошибку
        //        double YTriangle = 0;
        //        double XTriangle = 0;

        //        //шаг1 определение области попадания горизонталь или вертикаль
        //        //рисование в области х (объект 2 ниже или выше 1)

        //        if (x_area == true)  //попали в горизонталь
        //        {

        //            if (up2 > down1) // 2 ниже 1
        //            {
        //                Y1 = down1;
        //                Y2 = up2;
        //                maxY = Y2;
        //                minY = Y1;
        //                YTriangle = -10;

        //            }
        //            else if (down2 < up1) //2 выше 1
        //            {
        //                Y1 = up1;
        //                Y2 = down2;
        //                maxY = Y1;
        //                minY = Y2;
        //                YTriangle = 10;
        //            }

        //            if ((H1X <= C2X && C2X <= K1X) && (H2X <= C1X && C1X <= K2X))  //середины находятся в пределах фигур по х 
        //            {
        //                if (C2X <= C1X)//2 слева от 1
        //                {
        //                    maxX = C1X;
        //                    minX = C2X;
        //                }
        //                else   // 2 справа от 1
        //                {
        //                    maxX = C2X;
        //                    minX = C1X;
        //                }

        //                X = (maxX - minX) / 2 + minX;

        //                //РИСУЕМ ПРЯМУЮ ОТ 2 К 1
        //                //исправление на polyline
        //                Point1 = new Point(X, Y1);
        //                Point2 = new Point(X, Y2);
        //                Point3 = Point2;
        //                Point4 = Point2;

        //                //для стрелки                        

        //                if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
        //                {
        //                    PointTriangle1 = new Point(X - 10, Y2 + YTriangle);
        //                    PointTriangle2 = Point2;
        //                    PointTriangle3 = new Point(X + 10, Y2 + YTriangle);
        //                    PointTriangle4 = new Point(PointTriangle2.X, PointTriangle2.Y + YTriangle * 2);  //для ромба ассоциации
        //                }
        //                else
        //                {
        //                    PointTriangle1 = new Point(X - 10, Y1 - YTriangle);
        //                    PointTriangle2 = Point1;
        //                    PointTriangle3 = new Point(X + 10, Y1 - YTriangle);
        //                    PointTriangle4 = new Point(PointTriangle2.X, PointTriangle2.Y - YTriangle * 2);  //для ромба ассоциации
        //                }

        //                // Update_Arrow_universal(arrow, start, stop, PointTriangle1, PointTriangle2, PointTriangle3);
        //                Update_Rombe(rombe, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
        //            }
        //            else   //середины находятся за пределами фигур по х
        //            {
        //                //for polyline
        //                Y = ((maxY - minY) / 2) + minY;

        //                if (Y <= 0) //определение области рисования для линии (гориpонтальная = true)
        //                {
        //                    x_area = false;
        //                    y_area = true;
        //                }
        //                //РИСУЕМ СТУПЕНЬ ОТ 2 к 1  или 1 к 2?
        //                Point1 = new Point(C1X, Y1);
        //                Point2 = new Point(C1X, Y);
        //                Point3 = new Point(C2X, Y);
        //                Point4 = new Point(C2X, Y2);

        //                //для стрелки                        

        //                if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
        //                {
        //                    PointTriangle1 = new Point(C2X - 10, Y2 + YTriangle);
        //                    PointTriangle2 = new Point(C2X, Y2);
        //                    PointTriangle3 = new Point(C2X + 10, Y2 + YTriangle);
        //                    PointTriangle4 = new Point(PointTriangle2.X, PointTriangle2.Y + YTriangle * 2);  //для ромба ассоциации

        //                }
        //                else
        //                {
        //                    PointTriangle1 = new Point(C1X - 10, Y1 - YTriangle);
        //                    PointTriangle2 = new Point(C1X, Y1);
        //                    PointTriangle3 = new Point(C1X + 10, Y1 - YTriangle);
        //                    PointTriangle4 = new Point(PointTriangle2.X, PointTriangle2.Y - YTriangle * 2);  //для ромба ассоциации
        //                }

        //                Update_Rombe(rombe, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
        //            }
        //        }

        //        //рисование в области y(объект 2справа или слева 1)
        //        if (y_area == true)
        //        {
        //           // label_to_box.Text += Environment.NewLine + "Попали в y=" + y_area;
        //            if (left2 > right1) // 2 справа от 1
        //            {
        //                X1 = right1;
        //                X2 = left2;
        //                maxX = X2;
        //                minX = X1;
        //                XTriangle = -10;
        //            }
        //            else if (right2 < left1) //2 слева от 1
        //            {
        //                X1 = left1;
        //                X2 = right2;
        //                maxX = X1;
        //                minX = X2;
        //                XTriangle = 10;
        //            }

        //            if ((H1Y >= C2Y && C2Y >= K1Y) && (H2Y >= C1Y && C1Y >= K2Y))  //середины находятся в пределах фигур по Y
        //            {
        //                if (C2Y >= C1Y) //середина 2 ниже середины 1
        //                {
        //                    maxY = C2Y;
        //                    minY = C1Y;
        //                }
        //                else
        //                {
        //                    maxY = C1Y;
        //                    minY = C2Y;
        //                }

        //                Y = ((maxY - minY) / 2) + minY;

        //                //РИСУЕМ ПРЯМУЮ горизонтальную ОТ 2 к 1
        //                //for polyline
        //                Point1 = new Point(X1, Y);
        //                Point2 = new Point(X2, Y);
        //                Point3 = Point2;
        //                Point4 = Point2;

        //                //для стрелки                       

        //                if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
        //                {
        //                    PointTriangle1 = new Point(X2 + XTriangle, Y + 10);
        //                    PointTriangle2 = Point2;
        //                    PointTriangle3 = new Point(X2 + XTriangle, Y - 10);
        //                    PointTriangle4 = new Point(PointTriangle2.X+ XTriangle*2, PointTriangle2.Y);  //для ромба ассоциации
        //                }
        //                else
        //                {
        //                    PointTriangle1 = new Point(X1 - XTriangle, Y + 10);
        //                    PointTriangle2 = Point1;
        //                    PointTriangle3 = new Point(X1 - XTriangle, Y - 10);
        //                    PointTriangle4 = new Point(PointTriangle2.X - XTriangle * 2, PointTriangle2.Y);  //для ромба ассоциации
        //                }

        //                Update_Rombe(rombe, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
        //            }
        //            else
        //            {
        //                X = (maxX - minX) / 2 + minX;

        //                if (X <= 0)  //определение области рисования для линии (вертикальная = true)
        //                {
        //                    x_area = true;
        //                    y_area = false;
        //                }

        //                //РИСУЕМ СТУПЕНЬ горизонтальную ОТ 2 к 1
        //                Point1 = new Point(X1, C1Y);
        //                Point2 = new Point(X, C1Y);
        //                Point3 = new Point(X, C2Y);
        //                Point4 = new Point(X2, C2Y);

        //                //для стрелки                       

        //                if (l_reverce == true) // если l_reverce == true то объект 2 = базовый
        //                {
        //                    PointTriangle1 = new Point(X2 + XTriangle, C2Y + 10);
        //                    PointTriangle2 = new Point(X2, C2Y);
        //                    PointTriangle3 = new Point(X2 + XTriangle, C2Y - 10);
        //                    PointTriangle4 = new Point(PointTriangle2.X + XTriangle * 2, PointTriangle2.Y);  //для ромба ассоциации
        //                }
        //                else
        //                {
        //                    PointTriangle1 = new Point(X1 - XTriangle, C1Y + 10);
        //                    PointTriangle2 = new Point(X1, C1Y);
        //                    PointTriangle3 = new Point(X1 - XTriangle, C1Y - 10);
        //                    PointTriangle4 = new Point(PointTriangle2.X - XTriangle * 2, PointTriangle2.Y);  //для ромба ассоциации
        //                }
        //                Update_Rombe(rombe, start, stop, PointTriangle1, PointTriangle2, PointTriangle3, PointTriangle4);
        //            }
        //        }
        //    }
        //}

        void Update_line_universal(Polyline[,] line, int start, int stop, Point Point1, Point Point2, Point Point3, Point Point4)
        {
            line[start, stop].Points.RemoveAt(1 - 1);
            line[start, stop].Points.Insert(1 - 1, Point1);
            line[start, stop].Points.RemoveAt(2 - 1);
            line[start, stop].Points.Insert(2 - 1, Point2);
            line[start, stop].Points.RemoveAt(3 - 1);
            line[start, stop].Points.Insert(3 - 1, Point3);
            line[start, stop].Points.RemoveAt(4 - 1);
            line[start, stop].Points.Insert(4 - 1, Point4);
        }

        void Update_Arrow_universal(Polyline[,] arrow, int start, int stop, Point Point1, Point Point2, Point Point3)
        {
            arrow[start, stop].Points.RemoveAt(1 - 1);
            arrow[start, stop].Points.Insert(1 - 1, Point1);
            arrow[start, stop].Points.RemoveAt(2 - 1);
            arrow[start, stop].Points.Insert(2 - 1, Point2);
            arrow[start, stop].Points.RemoveAt(3 - 1);
            arrow[start, stop].Points.Insert(3 - 1, Point3);
        }

        void Update_Rombe(Polyline[,] rombe, int start, int stop, Point Point1, Point Point2, Point Point3, Point Point4)
        {
            rombe[start, stop].Points.RemoveAt(1 - 1);
            rombe[start, stop].Points.Insert(1 - 1, Point1);
            rombe[start, stop].Points.RemoveAt(2 - 1);
            rombe[start, stop].Points.Insert(2 - 1, Point2);
            rombe[start, stop].Points.RemoveAt(3 - 1);
            rombe[start, stop].Points.Insert(3 - 1, Point3);
            rombe[start, stop].Points.RemoveAt(4 - 1);
            rombe[start, stop].Points.Insert(4 - 1, Point4);
        }

        void Create_TypeLine()// (int number) //опледеляем тип линий для вссех зависимостей
        {
            int number = 0;
            int test = 0;
            for (int i = 0; i < relation1.Table.Count; i++)             // relation1.Table   здесь важно какой тип у других классов -> у findClass[number].Type
                for (int j = 0; j < relation1.Table[i].Count; j++)
                {
                    number = relation1.Table[i][j];
                    test= relation1.Table[i][j]+1;
                    if ( findClass [number].Type == "abstract")
                    {
                        label1[number].Background = Brushes.MediumPurple;
                    }

                    if (findClass[number].Type == "interface")
                    {
                        label1[number].Background = Brushes.LightBlue ;
                        if (j!=0)  // линии lineRelation[i, 0] не инициализировано
                        { 
                            int p1 = relation1.Table[i][0];
                            int p2 = relation1.Table[i][j];
                            lineRelation[p1, p2].StrokeDashArray = new DoubleCollection() { 2 }; //тип линии = пунктирная   нужно отрисовывать
                        }
                    }
                }
        }

        void UpdatePosition(MouseEventArgs e)
        {
            var point = e.GetPosition(DragArena);
            var newPos = point - relativeMousePos;
            Canvas.SetLeft(draggedObject, newPos.X);
            Canvas.SetTop(draggedObject, newPos.Y);
        }

        void OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            FinishDrag(sender, e);
            Mouse.Capture(null);
        }

        void OnLostCapture(object sender, MouseEventArgs e)
        {
            FinishDrag(sender, e);
        }

        void FinishDrag(object sender, MouseEventArgs e)
        {
            draggedObject.MouseMove -= OnDragMove;
            draggedObject.LostMouseCapture -= OnLostCapture;
            draggedObject.MouseUp -= OnMouseUp;
            UpdatePosition(e);
        }

        #endregion

        #region //МАШТАБИРОВАНИЕ CANVAS  нажать ctrl+scroll

        private void DragArena_MouseDown_1(object sender, MouseButtonEventArgs e)
        {                    
        }

        private void DragArena_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)// KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
                Scroll_DragArena.SetValue(ScrollViewer.VerticalScrollBarVisibilityProperty, ScrollBarVisibility.Disabled);  //отключаем скрол
        }

        private void DragArena_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)// KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
                Scroll_DragArena.SetValue(ScrollViewer.VerticalScrollBarVisibilityProperty, ScrollBarVisibility.Visible); //включаем скролл
        }

        double ScaleRate = 1;   
        int count_plus = 1;     
        ScaleTransform st = new ScaleTransform();

        private void canvas1_MouseWheel(object sender, MouseWheelEventArgs e) //прокрутка колесом для маштабирования канваса
        {
            DragArena.Focus();//фокус на драгарену чтобы заработал обработчик клавиши. работает совместно с        Xalm Focusable="True"

            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                Canvas c = sender as Canvas;
                st = new ScaleTransform();

                if (e.Delta > 0) //колесо вверх
                {
                    ScaleRate += 0.05;
                    st.ScaleX *= ScaleRate;
                    st.ScaleY *= ScaleRate;
              
                    if (DragArena.Height >= 1500)//ограничим уменьшение канваса при приближении
                    {
                        DragArena.Height = DragArena.Height - (1000 * 5 / 100);   // ScaleRate;// = DragArena.Height* (ScaleRate*0.95); 
                        DragArena.Width = DragArena.Width - (1000 * 5 / 100);//  ScaleRate; //;= DragArena.Width * (ScaleRate*0.95);
                    }
                    else
                    {
                        count_plus++; // когда канвас меньше 1000 будет считаться буфер колесика, сколько раз потом не нужно увеличивать канвас
                    }
                }
                else  //колесо вниз
                {
                    ScaleRate -= 0.05;
                    st.ScaleX *= ScaleRate; //st.ScaleX /= ScaleRate;
                    st.ScaleY *= ScaleRate; //st.ScaleY /= ScaleRate;
            
                    if (count_plus > 1)
                    {
                        count_plus--; //очищение "буфера колеса" чтобы  канвас начал увеличиваться с > 1000 c той же картинки
                    }
                    else
                    {
                        //1 шаг равен 5 % 
                        DragArena.Height = DragArena.Height + (1000 * 5 / 100);//ScaleRate;// DragArena.Height / (ScaleRate *0.95);
                        DragArena.Width = DragArena.Width + (1000 * 5 / 100);// ScaleRate; //DragArena.Width / (ScaleRate*0.95 );
                    }
                }
                c.LayoutTransform = st;  // c.RenderTransform = st;
            }

        }
        #endregion
        private void Align_Click(object sender, RoutedEventArgs e) //кнопка - компоновка label на канвасе
        {           
            get_relation(); //вывод таблицы
            Create_TypeLine();

            // *5 устанавливем положение на канвасе   
            double xx = 0;
            double y_max = 0;
            
            for (int i = 0; i < relation1.TableReverce.Count; i++)
            {
                y_canvas[i] =  new double();
                y_canvas[i] = 0;
                if (i==0)
                    y_canvas[i] = y_canvas[i] + y_max + 20; //20 зазор 
                else
                y_canvas[i] = y_canvas[i-1] + y_max + 20; //20 зазор 

                xx = 20;
                y_max = 0;
                
                for (int j = 0; j < relation1.TableReverce[i].Count; j++)
                {
                    if ( (label1[relation1.TableReverce[i][j]].ActualHeight > y_max) )
                        y_max = label1[relation1.TableReverce[i][j]].ActualHeight;

                    Point point = new Point(xx, (y_canvas[i]));

                    if (relation1.TableReverce[i].Count == 1)
                        point = new Point(500, (y_canvas[i]));
               
                    Canvas.SetLeft(label1[relation1.TableReverce[i][j]], point.X);// Convert.ToDouble(i * 30));//вариант 2
                    Canvas.SetTop(label1[relation1.TableReverce[i][j]], point.Y);//Convert.ToDouble(i * 30));//вариант 2

                    xx = xx + label1[relation1.TableReverce[i][j]].ActualWidth + 20; //вывод канвас на позицию x
                }
            }

            //удаляем пустые области
            bool l = false;
            double yi = 0;
            for (int ai = 0; ai < relation1.TableReverce.Count; ai++)
            {
                l = false;
                for (int bi = 0; bi < relation1.TableReverce.Count; bi++)
                    for (int bj = 1; bj < relation1.TableReverce[bi].Count; bj++)
                    {
                        if (l == false)
                            if (relation1.TableReverce[ai][0] == relation1.TableReverce[bi][bj])
                            {
                                l = true;
                                int test = 0;
                                for (int ci = ai + 1; ci < relation1.TableReverce.Count; ci++)
                                {
                                    for (int cj = 0; cj < relation1.TableReverce[ci].Count; cj++)
                                    {                                    
                                        yi = y_canvas[ci] - label1[relation1.TableReverce[ai][0]].ActualHeight;

                                        test++; 
                                        Canvas.SetTop(label1[relation1.TableReverce[ci][cj]], yi);
                                    }
                                    y_canvas[ci] = yi - 20; //20 удаляем зазор
                                }                             
                            }
                    }
            }
        }

        public void Align() //метод - компоновка label на канвасе
        { 
            get_relation(); //вывод таблицы

            Create_TypeLine();
            // *5 устанавливем положение на канвасе   
            double xx = 0;
            double y_max = 0;

            for (int i = 0; i < relation1.TableReverce.Count; i++)
            {
                y_canvas[i] = new double();
                y_canvas[i] = 0;
                if (i == 0)
                    y_canvas[i] = y_canvas[i] + y_max + 20; //20 зазор (Верхний зазор первого блока диаграммы)
                else
                    y_canvas[i] = y_canvas[i - 1] + y_max + 200; //20 зазор 

                xx = 20;
                y_max = 0;

                for (int j = 0; j < relation1.TableReverce[i].Count; j++)
                {
                    if ((label1[relation1.TableReverce[i][j]].ActualHeight > y_max))
                        y_max = label1[relation1.TableReverce[i][j]].ActualHeight;

                    Point point = new Point(xx, (y_canvas[i]));

                    if (relation1.TableReverce[i].Count == 1)
                        point = new Point(500, (y_canvas[i]));
               
                    Canvas.SetLeft(label1[relation1.TableReverce[i][j]], point.X);// Convert.ToDouble(i * 30));//вариант 2
                    Canvas.SetTop(label1[relation1.TableReverce[i][j]], point.Y);//Convert.ToDouble(i * 30));//вариант 2
                    xx = xx + label1[relation1.TableReverce[i][j]].ActualWidth + 20; //вывод канвас на позицию x
                }
            }

            //удаляем пустые области
            bool l = false;
            double yi = 0;
            for (int ai = 0; ai < relation1.TableReverce.Count; ai++)
            {
                l = false;
                for (int bi = 0; bi < relation1.TableReverce.Count; bi++)
                    for (int bj = 1; bj < relation1.TableReverce[bi].Count; bj++)
                    {
                        if (l == false)
                            if (relation1.TableReverce[ai][0] == relation1.TableReverce[bi][bj])
                            {
                                l = true;
                                int test = 0;
                                for (int ci = ai + 1; ci < relation1.TableReverce.Count; ci++)
                                {
                                    for (int cj = 0; cj < relation1.TableReverce[ci].Count; cj++)
                                    {
                                        yi = y_canvas[ci] - label1[relation1.TableReverce[ai][0]].ActualHeight;
                                        test++;
                                        Canvas.SetTop(label1[relation1.TableReverce[ci][cj]], yi);
                                    }
                                    y_canvas[ci] = yi - 20; //20 удаляем зазор
                                }
                            }
                    }
            }
        }

        #region //скрытие и отображения линий

        private void CheckBoxDependency_Checked(object sender, RoutedEventArgs e)
        {
            for (int a = 0; a < relation1.Table.Count; a++)
            {
                List<int> str = relation1.Table[a];
                for (int b = 1; b < relation1.Table[a].Count; b++)
                {
                    int i = relation1.Table[a][0];
                    int j = relation1.Table[a][b];
                    lineRelation[i, j].Visibility = Visibility.Visible;
                    polylineArrowDependent[i, j].Visibility = Visibility.Visible;
                }
            }
        }

        private void CheckBoxDependency_Unchecked(object sender, RoutedEventArgs e)
        {
            for (int a = 0; a < relation1.Table.Count; a++)
            {
                List<int> str = relation1.Table[a];
                for (int b = 1; b < relation1.Table[a].Count; b++)
                {
                    int i = relation1.Table[a][0];
                    int j = relation1.Table[a][b];
                    lineRelation[i, j].Visibility = Visibility.Hidden;
                    polylineArrowDependent[i, j].Visibility = Visibility.Hidden;
                }
            }
        }

        private void CheckBoxComposition_Checked(object sender, RoutedEventArgs e)
        {
            for (int a = 0; a < relation1.TableComposition.Count; a++)
            {
                List<int> str = relation1.TableComposition[a];
                for (int b = 1; b < relation1.TableComposition[a].Count; b++)
                {            
                    int i = relation1.TableComposition[a][0];
                    int j = relation1.TableComposition[a][b];
                    lineAssociation[i, j].Visibility = Visibility.Visible;
                    polylineArrowAssociation[i, j].Visibility = Visibility.Visible;                  
                }
            }

            for (int a = 0; a < relation1.TableCompositionReverce.Count; a++)
            {
                List<int> str = relation1.TableCompositionReverce[a];
                for (int b = 1; b < relation1.TableCompositionReverce[a].Count; b++)
                {
                    int i = relation1.TableCompositionReverce[a][0];
                    int j = relation1.TableCompositionReverce[a][b];
                    polylineRombeAssociation[i, j].Visibility = Visibility.Visible;
                }
            }
        }
        
        private void CheckBoxComposition_Unchecked(object sender, RoutedEventArgs e)
        {
            for (int a = 0; a < relation1.TableComposition.Count; a++)
            {
                List<int> str = relation1.TableComposition[a];
                for (int b = 1; b < relation1.TableComposition[a].Count; b++)
                {
                    int i = relation1.TableComposition[a][0];
                    int j = relation1.TableComposition[a][b];
                    lineAssociation[i, j].Visibility = Visibility.Hidden;
                    polylineArrowAssociation[i, j].Visibility = Visibility.Hidden;               
                }
            }


            for (int a = 0; a < relation1.TableCompositionReverce.Count; a++)
            {
                List<int> str = relation1.TableCompositionReverce[a];
                for (int b = 1; b < relation1.TableCompositionReverce[a].Count; b++)
                {
                    int i = relation1.TableCompositionReverce[a][0];
                    int j = relation1.TableCompositionReverce[a][b];
                    polylineRombeAssociation[i, j].Visibility = Visibility.Hidden;
                }
            }
        }

        private void CheckBoxAgregation_Checked(object sender, RoutedEventArgs e)
        {
            for (int a = 0; a < relation1.TableAgregation.Count; a++)
            {
                List<int> str = relation1.TableAgregation[a];
                for (int b = 1; b < relation1.TableAgregation[a].Count; b++)
                {
                    int i = relation1.TableAgregation[a][0];
                    int j = relation1.TableAgregation[a][b];
                    lineAgregation[i, j].Visibility = Visibility.Visible;
                    polylineArrowAgregation[i, j].Visibility = Visibility.Visible;
                }
            }

            for (int a = 0; a < relation1.TableAgregationReverce.Count; a++)
            {
                List<int> str = relation1.TableAgregationReverce[a];
                for (int b = 1; b < relation1.TableAgregationReverce[a].Count; b++)
                {
                    int i = relation1.TableAgregationReverce[a][0];
                    int j = relation1.TableAgregationReverce[a][b];
                    polylineRombeAgregation[i, j].Visibility = Visibility.Visible;
                }
            }
        }

        private void CheckBoxAgregation_Unchecked(object sender, RoutedEventArgs e)
        {
            for (int a = 0; a < relation1.TableAgregation.Count; a++)
            {
                List<int> str = relation1.TableAgregation[a];

                for (int b = 1; b < relation1.TableAgregation[a].Count; b++)
                {
                    int i = relation1.TableAgregation[a][0];
                    int j = relation1.TableAgregation[a][b];
                    lineAgregation[i, j].Visibility = Visibility.Hidden;
                    polylineArrowAgregation[i, j].Visibility = Visibility.Hidden;
                }
            }

            for (int a = 0; a < relation1.TableAgregationReverce.Count; a++)
            {
                List<int> str = relation1.TableAgregationReverce[a];
                for (int b = 1; b < relation1.TableAgregationReverce[a].Count; b++)
                {
                    int i = relation1.TableAgregationReverce[a][0];
                    int j = relation1.TableAgregationReverce[a][b];
                    polylineRombeAgregation[i, j].Visibility = Visibility.Hidden;
                }
            }
        }
        #endregion

        public void create_label() //создание label и наполнение данными класса , добавление на канвас
        {
            Point P1, P2;
            //создание лабелов
            for (int i = 0; i <= analyze1.count_class; i++)    //analyze1.count_class равен количество -1   //было for (int i = 0; i < label1.Length; i++)
            {
                label1[i] = new Label(); //тест перенос в инициализацию ?
                                         // label1[i].Name = "label" + i;  можно не именовать лайбел

                label1[i].Content += findClass[i].NameClass+"       " +findClass[i].Type; ;  //имя класса

                label1[i].Content += Environment.NewLine + findClass[i].Namespace;// namespace

                String symbolString = new string('_', 35);
                label1[i].Content += Environment.NewLine + symbolString;
                
                //вывод field   поля
                for (int f = 0; f < findClass[i].Field_Count; f++)
                {
                    label1[i].Content += Environment.NewLine + String_Class_Visibility(Convert.ToString(findClass[i].Field[f][0])); //+String_Class_Visibility(Convert.ToString(findClass[i].Field[f][0]));
                }

                label1[i].Content += Environment.NewLine + symbolString;

                //вывод metod    методы  
                for (int m = 0; m < findClass[i].Metod_Count; m++)
                {
                    label1[i].Content += Environment.NewLine + String_Class_Visibility(Convert.ToString(findClass[i].Metod[m][0])); //+String_Class_Visibility(Convert.ToString(findClass[i].Metod[m][0]));
                }

                label1[i].MouseDown += new System.Windows.Input.MouseButtonEventHandler(Btn_OnMouseDown);
                label1[i].MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(StartDrag); // вариант 2 из метода для рисованных фигур  работает для лейбла и элипса
                label1[i].MouseUp += new System.Windows.Input.MouseButtonEventHandler(Btn_OnMouseUp);
                label1[i].Tag = i; //добавляем таг для лабла для его последующего поиска 
                label1[i].Background = Brushes.LightSkyBlue; //цвет для лейбла //тест перенос в инициализацию

                // *5 устанавливем положение на канвасе   
                rundom[i] = rnd.Next(400);
                x[i] = Convert.ToDouble(i * 30);
                y[i] = Convert.ToDouble(i * 30 + rundom[i]);
                Point pointLine = new Point(x[i], y[i]);
                Canvas.SetLeft(label1[i], pointLine.X);// Convert.ToDouble(i * 30));//вариант 2
                Canvas.SetTop(label1[i], pointLine.Y);//Convert.ToDouble(i * 30));//вариант 2
                DragArena.Children.Add(label1[i]);  
            }
        }

        public string String_Class_Visibility(string string_input)//форматирование строки при выводе информации о классе в лейбл
        {
            string result;
            Regex regex_del = new Regex(@"^\s*|[\/]\s*\w*");//замена и удаление пробелов вначале строки и комментариев
            Regex regex_rep = new Regex(@"^public|^private|^protected|^internal");// + - #  ~
            Regex regex_md = new Regex(@"^\w+");

            string string_regex_del = regex_del.Replace(string_input, String.Empty);

            switch (regex_md.Match(string_regex_del).Value)
            {
                case "public":
                    result = regex_rep.Replace(string_regex_del, "+");
                    break;
                case "private":
                    result = regex_rep.Replace(string_regex_del, "-");
                    break;
                case "protected":
                    result = regex_rep.Replace(string_regex_del, "#");
                    break;
                case "internal":
                    result = regex_rep.Replace(string_regex_del, "~");
                    break;
                    default:       // по умолчанию поля private
                    result = String.Concat("- ", string_regex_del);
                    break;
            }           
            return result;
        }

        public void draw_relation()  //рисует прямые линии только при создании лабелов
        {
            //очищаем массив точек
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 10; j++)
                { 
                    lineRelation[i, j].Points.Clear();
                }

            int first_class, from_class;
            Point P1, P2;
            for (int i = 0; i < relation1.Table.Count; i++)  //поиск в таблице зависимости
            {
                List<int> str = relation1.Table[i];  //поиск в таблице зависимости
                for (int j = 1; j < str.Count; j++)
                {
                    first_class = str[0];
                    from_class = str[j];
                    if (from_class != 0) //если есть наследование  //анализировать и удалить лишнее
                    {
                        //получаем координаты объектов

                        Vector first_vector = VisualTreeHelper.GetOffset(label1[first_class]);
                        Vector second_vector = VisualTreeHelper.GetOffset(label1[from_class]);
                        // Convert the vector to a point value.
                        Point first_Point = new Point(first_vector.X, first_vector.Y);
                        Point second_Point = new Point(second_vector.X, second_vector.Y);
                        // *7 рисуем линию. тест
                        lineRelation[first_class, from_class].Stroke = System.Windows.Media.Brushes.LightSteelBlue; //было // lineRelation[i, j].Stroke = System.Windows.Media.Brushes.LightSteelBlue;

                        lineRelation[first_class, from_class].Points.Add(first_Point); //1я точка
                        lineRelation[first_class, from_class].Points.Add(second_Point);//2я точка
                        lineRelation[first_class, from_class].Points.Add(second_Point);//3я точка
                        lineRelation[first_class, from_class].Points.Add(second_Point);//4я точка

                       // //для треугольника
                        //здесь можно изменить свойства линии                    
                        lineRelation[first_class, from_class].HorizontalAlignment = HorizontalAlignment.Left;
                        lineRelation[first_class, from_class].VerticalAlignment = VerticalAlignment.Center;
                        lineRelation[first_class, from_class].StrokeThickness = 2;
                        lineRelation[first_class, from_class].AllowDrop = true;
                    }
                }
            }
        }

        public void get_relation()   //вывод таблицы зависимости в левую панель textBox
        {
            for (int i = 0; i < relation1.Table.Count; i++)
            {
              List<int> str = relation1.Table[i];
              for (int j = 0; j < relation1.Table[i].Count; j++)
              {
              }
            }
            for (int i = 0; i < relation1.TableReverce.Count; i++)
            {
                List<int> str = relation1.TableReverce[i];
                for (int j = 0; j < relation1.TableReverce[i].Count; j++)
                {
                }
            }
        }

        #region //сохранение канваса в картинку
        private void Menu_Click_SaveImage(object sender, RoutedEventArgs e) //кнопка сохранить
        {
            Microsoft.Win32.SaveFileDialog saveimg = new Microsoft.Win32.SaveFileDialog();
            saveimg.DefaultExt = ".PNG";
            saveimg.Filter = "Image (.PNG)|*.PNG";
            if (saveimg.ShowDialog() == true)
            {
                ToImageSource(DragArena, saveimg.FileName);
            }
        }

        public void ToImageSource(Canvas canvas, string filename) //метод сохранение файла в картинку
        {
            //качество изображения
            double dpi = 300;
            double scale = dpi / 96;
            RenderTargetBitmap image = new RenderTargetBitmap((int)(canvas.ActualWidth * scale), (int)(canvas.ActualHeight * scale), dpi, dpi, PixelFormats.Pbgra32);
            canvas.Measure(new Size((int)canvas.ActualWidth, (int)canvas.ActualHeight));  //в этом случае идет сохранение видимого канваса
            canvas.Arrange(new Rect(new Size((int)canvas.ActualWidth, (int)canvas.ActualHeight)));

            image.Render(canvas);
            PngBitmapEncoder encoder = new PngBitmapEncoder();  //конвертируем в png формат
            encoder.Frames.Add(BitmapFrame.Create(image));
            using (FileStream file = File.Create(filename))
            {
                encoder.Save(file);
            }    
        }
        #endregion

        private void Metrics_Click(object sender, RoutedEventArgs e)
        {
            formMetrics form1 = new formMetrics(analyze1); //создаем окно
            form1.ShowDialog();
        }

        private void File_Open_Click(object sender, EventArgs e)//кнопка открыть
        {
            System.Windows.Forms.FolderBrowserDialog folder= new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = folder.ShowDialog();

            if (!string.IsNullOrWhiteSpace(folder.SelectedPath))
            {
                string[] files = Directory.GetFiles(folder.SelectedPath, "*",SearchOption.AllDirectories);

                OpenFileDialog SelectFolder = new OpenFileDialog (files); //создаем окно и передаем список файлов  форму
                SelectFolder.ShowDialog();
                // var di = SelectFolder.dataInput;
                #region //поиск в большом тексте перенесен в метод кнопки    private void File_Open_Click(object sender, EventArgs e)

                analyze1 = new Analyze(SelectFolder.dataInput.path_text, SelectFolder.dataInput.path_field, SelectFolder.dataInput.path_2_ToMask);  //создаем и анализируем 1 текст
                 
                analyze1.GetInfoOfClass();

                for (int i = 0; i <= analyze1.count_class; i++) //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!пауза
                {
                    findClass.Add(new FindClass());
                    findClass[i] = analyze1.analyzeFindClass[i]; //             
                }
                relation1 = analyze1.relationAnalyze; //переброс из методов анализа 
                InicializationLine();
                create_label();
                Align();//упорядочить

                #endregion                    
            }
        }

        public void InicializationLine()//инициализация линий, количество определяется по таблице 
        {
            //пример инициализации по таблице
            //выводится только где больше 2х элементов в строке
            //    1 и 3, 2 и 1, ... 8 и 3, 8 и 4

            //таблица
            //    1 - 3 -
            //    2 - 1 -
            //    3 -
            //    4 -
            //    5 - 3 -
            //    6 - 3 -
            //    7 -
            //    8 - 3 - 4 -
            //    9 - 4 -
            //    10 -

            //инициализируем линии зависимости для отрисовки в дальнейшем

            for (int a = 0; a < relation1.Table.Count; a++)
            {
                List<int> str = relation1.Table[a];
                for (int b = 1; b < relation1.Table[a].Count; b++)
                {
                    int i = relation1.Table[a][0];
                    int j = relation1.Table[a][b];
                    lineRelation[i, j] = new Polyline();
                    lineRelation[i, j].Points.Add(new Point(0, 0)); //1я точка
                    lineRelation[i, j].Points.Add(new Point(0, 0));//2я точка
                    lineRelation[i, j].Points.Add(new Point(0, 0));//3я точка
                    lineRelation[i, j].Points.Add(new Point(0, 0));//4я точка
                    lineRelation[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    lineRelation[i, j].VerticalAlignment = VerticalAlignment.Center;
                    lineRelation[i, j].StrokeThickness = 2;
                    lineRelation[i, j].AllowDrop = true;
                    lineRelation[i, j].Stroke = System.Windows.Media.Brushes.Green;
                    //  lineRelation[i, j].ZIndex = 3;
                    DragArena.Children.Add(lineRelation[i, j]);
                }
            }

            //инициализируем стрелки зависимости для отрисовки в дальнейшем
            for (int a = 0; a < relation1.Table.Count; a++)
            {
                List<int> str = relation1.Table[a];
                for (int b = 1; b < relation1.Table[a].Count; b++)
                {
                    int i = relation1.Table[a][0];
                    int j = relation1.Table[a][b];
                    polylineArrowDependent[i, j] = new Polyline();
                    polylineArrowDependent[i, j].Points.Add(new Point(0, 0)); //1я точка
                    polylineArrowDependent[i, j].Points.Add(new Point(0, 0));//2я точка
                    polylineArrowDependent[i, j].Points.Add(new Point(0, 0));//3я точка  всего 3 точки
                 // polylineArrowDependent[i, j].Points.Add(new Point(0, 0));//4я точка
                    polylineArrowDependent[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    polylineArrowDependent[i, j].VerticalAlignment = VerticalAlignment.Center;
                    polylineArrowDependent[i, j].StrokeThickness = 2;
                    polylineArrowDependent[i, j].AllowDrop = true;
                    polylineArrowDependent[i, j].Stroke = System.Windows.Media.Brushes.Green;//Red;
                    polylineArrowDependent[i, j].Fill = Brushes.Green;// Red;
                    DragArena.Children.Add(polylineArrowDependent[i, j]);
                }
            }

            #region//инициализация линий для Ассоциации - Поменять на композицию
            //инициализируем линии зависимости Ассоциации для отрисовки в дальнейшем
            for (int a = 0; a < relation1.TableComposition.Count; a++)
            {
                List<int> str = relation1.TableComposition[a];
                for (int b = 1; b < relation1.TableComposition[a].Count; b++)
                {
                    int i = relation1.TableComposition[a][0];
                    int j = relation1.TableComposition[a][b];

                    lineAssociation[i, j] = new Polyline();
                    lineAssociation[i, j].Points.Add(new Point(0, 0)); //1я точка
                    lineAssociation[i, j].Points.Add(new Point(0, 0));//2я точка
                    lineAssociation[i, j].Points.Add(new Point(0, 0));//3я точка
                    lineAssociation[i, j].Points.Add(new Point(0, 0));//4я точка
                    lineAssociation[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    lineAssociation[i, j].VerticalAlignment = VerticalAlignment.Center;
                    lineAssociation[i, j].StrokeThickness = 2;
                    lineAssociation[i, j].AllowDrop = true;
                    lineAssociation[i, j].Stroke = System.Windows.Media.Brushes.Blue;
                    DragArena.Children.Add(lineAssociation[i, j]);
                }
            }

            //инициализируем стрелки Ассоциации зависимости для отрисовки в дальнейшем
            for (int a = 0; a < relation1.TableComposition.Count; a++)
            {
                List<int> str = relation1.TableComposition[a];
                for (int b = 1; b < relation1.TableComposition[a].Count; b++)
                {
                    int i = relation1.TableComposition[a][0];
                    int j = relation1.TableComposition[a][b];
                    polylineArrowAssociation[i, j] = new Polyline();
                    polylineArrowAssociation[i, j].Points.Add(new Point(0, 0)); //1я точка
                    polylineArrowAssociation[i, j].Points.Add(new Point(0, 0));//2я точка
                    polylineArrowAssociation[i, j].Points.Add(new Point(0, 0));//3я точка  всего 3 точки
                 // polylineArrowAssociation[i, j].Points.Add(new Point(0, 0));//4я точка
                    polylineArrowAssociation[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    polylineArrowAssociation[i, j].VerticalAlignment = VerticalAlignment.Center;
                    polylineArrowAssociation[i, j].StrokeThickness = 2;
                    polylineArrowAssociation[i, j].AllowDrop = true;
                    polylineArrowAssociation[i, j].Stroke = System.Windows.Media.Brushes.Blue;
                    DragArena.Children.Add(polylineArrowAssociation[i, j]);
                }
            }

            //инициализируем ромб Ассоциации реверс  для отрисовки в дальнейшем
            for (int a = 0; a < relation1.TableCompositionReverce.Count; a++)
            {
                List<int> str = relation1.TableCompositionReverce[a];
                for (int b = 1; b < relation1.TableCompositionReverce[a].Count; b++)
                {
                    int i = relation1.TableCompositionReverce[a][0];
                    int j = relation1.TableCompositionReverce[a][b];
                    polylineRombeAssociation[i, j] = new Polyline();
                    polylineRombeAssociation[i, j].Points.Add(new Point(0, 0)); //1я точка
                    polylineRombeAssociation[i, j].Points.Add(new Point(0, 0));//2я точка
                    polylineRombeAssociation[i, j].Points.Add(new Point(0, 0));//3я точка  всего 3 точки
                    polylineRombeAssociation[i, j].Points.Add(new Point(0, 0));//4я точка
                    polylineRombeAssociation[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    polylineRombeAssociation[i, j].VerticalAlignment = VerticalAlignment.Center;
                    polylineRombeAssociation[i, j].StrokeThickness = 2;
                    polylineRombeAssociation[i, j].AllowDrop = true;
                    polylineRombeAssociation[i, j].Stroke = System.Windows.Media.Brushes.Red;
                    polylineRombeAssociation[i, j].Fill = Brushes.Red;   //  Fill  отключено                                                                                                  //  lineRelation[i, j].ZIndex = 3;
                    DragArena.Children.Add(polylineRombeAssociation[i, j]);
                }
            }
            #endregion

            #region //инициализация Агрегации
            //инициализация линий Агрегации
            //инициализируем линии зависимости Агрегации для отрисовки в дальнейшем
            for (int a = 0; a < relation1.TableAgregation.Count; a++)
            {
                List<int> str = relation1.TableAgregation[a];
                for (int b = 1; b < relation1.TableAgregation[a].Count; b++)
                {
                    int i = relation1.TableAgregation[a][0];
                    int j = relation1.TableAgregation[a][b];
                    lineAgregation[i, j] = new Polyline();
                    lineAgregation[i, j].Points.Add(new Point(0, 0)); //1я точка
                    lineAgregation[i, j].Points.Add(new Point(0, 0));//2я точка
                    lineAgregation[i, j].Points.Add(new Point(0, 0));//3я точка
                    lineAgregation[i, j].Points.Add(new Point(0, 0));//4я точка
                    lineAgregation[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    lineAgregation[i, j].VerticalAlignment = VerticalAlignment.Center;
                    lineAgregation[i, j].StrokeThickness = 2;
                    lineAgregation[i, j].AllowDrop = true;
                    lineAgregation[i, j].Stroke = System.Windows.Media.Brushes.Gray;
                    DragArena.Children.Add(lineAgregation[i, j]);
                }
            }

            //инициализируем стрелки Ассоциации зависимости для отрисовки в дальнейшем
            for (int a = 0; a < relation1.TableAgregation.Count; a++)
            {
                List<int> str = relation1.TableAgregation[a];

                for (int b = 1; b < relation1.TableAgregation[a].Count; b++)
                {
                    int i = relation1.TableAgregation[a][0];
                    int j = relation1.TableAgregation[a][b];
                    polylineArrowAgregation[i, j] = new Polyline();
                    polylineArrowAgregation[i, j].Points.Add(new Point(0, 0)); //1я точка
                    polylineArrowAgregation[i, j].Points.Add(new Point(0, 0));//2я точка
                    polylineArrowAgregation[i, j].Points.Add(new Point(0, 0));//3я точка  всего 3 точки
                 // polylineArrowAssociation[i, j].Points.Add(new Point(0, 0));//4я точка
                    polylineArrowAgregation[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    polylineArrowAgregation[i, j].VerticalAlignment = VerticalAlignment.Center;
                    polylineArrowAgregation[i, j].StrokeThickness = 2;
                    polylineArrowAgregation[i, j].AllowDrop = true;
                    polylineArrowAgregation[i, j].Stroke = System.Windows.Media.Brushes.Gray;
                    DragArena.Children.Add(polylineArrowAgregation[i, j]);
                }
            }

            //инициализируем ромб Ассоциации реверс  для отрисовки в дальнейшем
            for (int a = 0; a < relation1.TableAgregationReverce.Count; a++)
            {
                List<int> str = relation1.TableAgregationReverce[a];
                for (int b = 1; b < relation1.TableAgregationReverce[a].Count; b++)
                {
                    int i = relation1.TableAgregationReverce[a][0];
                    int j = relation1.TableAgregationReverce[a][b];
                    polylineRombeAgregation[i, j] = new Polyline();
                    polylineRombeAgregation[i, j].Points.Add(new Point(0, 0)); //1я точка
                    polylineRombeAgregation[i, j].Points.Add(new Point(0, 0));//2я точка
                    polylineRombeAgregation[i, j].Points.Add(new Point(0, 0));//3я точка  всего 3 точки
                    polylineRombeAgregation[i, j].Points.Add(new Point(0, 0));//4я точка
                    polylineRombeAgregation[i, j].HorizontalAlignment = HorizontalAlignment.Left;
                    polylineRombeAgregation[i, j].VerticalAlignment = VerticalAlignment.Center;
                    polylineRombeAgregation[i, j].StrokeThickness = 2;
                    polylineRombeAgregation[i, j].AllowDrop = true;
                    polylineRombeAgregation[i, j].Stroke = System.Windows.Media.Brushes.White;
                    polylineRombeAgregation[i, j].Fill = Brushes.White;                              //  Fill  отключено
                    DragArena.Children.Add(polylineRombeAgregation[i, j]);
                }
                #endregion
            }
        }

        #region методы в проекте

        private void Edit_Click(object sender, RoutedEventArgs e) //кнопка Edit , удаляет пуствые области при копановки лайбелов неправильно работает
        {
            //удаляем пустые строки
            bool l = false;
            double yi = 0;
            for (int ai = 0; ai < relation1.TableReverce.Count; ai++)
            {
                l = false;
                for (int bi = 0; bi < relation1.TableReverce.Count; bi++)
                    for (int bj = 1; bj < relation1.TableReverce[bi].Count; bj++)
                    {
                        if (l == false)
                            if (relation1.TableReverce[ai][0] == relation1.TableReverce[bi][bj])
                            {
                                l = true;
                              //  textBox_relation.Text += Environment.NewLine + "l=true " + (relation1.TableReverce[ai][0] + 1);
                                int test = 0;
                                for (int ci = ai + 1; ci < relation1.TableReverce.Count; ci++)
                                {
                                    for (int cj = 0; cj < relation1.TableReverce[ci].Count; cj++)
                                    {
                                        yi = y_canvas[ci] - label1[relation1.TableReverce[ai][0]].ActualHeight;
                                        test++;
                                        Canvas.SetTop(label1[relation1.TableReverce[ci][cj]], yi);
                                    }
                                    y_canvas[ci] = yi - 20; //20 удаляем зазор
                                }
                            }
                    }
            }
        }
        #endregion        
    }
}