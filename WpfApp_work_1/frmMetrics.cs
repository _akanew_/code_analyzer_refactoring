﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

using WpfApp_work_1.Data;

namespace WpfApp_work_1
{
    public partial class formMetrics : Form
    {
        Analyze analyze;
        DataTable dt;
        PackMetrics pm;

        public formMetrics(Analyze _analyze)
        {
            InitializeComponent();
            analyze = _analyze;

            pm = new PackMetrics(analyze);

            dataGridView1.DataSource = null;
            dataGridView1.DataMember = "";
            //dataGridView1.DataSource = new DataB pm.packages;
            dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("N");
            dt.Columns.Add("NA");
            dt.Columns.Add("A");
            dt.Columns.Add("I");
            dt.Columns.Add("D");
            foreach (Package pk in pm.packages.Values)
            {
                DataRow dr = dt.NewRow();
                dr["Name"] = pk.Name;
                dr["N"] = pk.N;
                dr["NA"] = pk.NA;
                dr["A"] = pk.A;
                dr["I"] = pk.I;
                dr["D"] = pk.D;
                dt.Rows.Add(dr);
            }

            dataGridView1.DataSource = dt;    
            dataGridView1.Refresh();
            zedGraphControl1_Load(this, null);
        }

        private void zedGraphControl1_Load(object sender, EventArgs e)
        {
            GraphPane pane = zedGraphControl1.GraphPane;
            pane.Title = "Pack Metrics";
            pane.XAxis.Max = 1.1;
            pane.XAxis.Min = -0.1;
            pane.YAxis.Min = -0.1;
            pane.YAxis.Max = 1.1;

            zedGraphControl1.AxisChange();

            PointPairList list0 = new PointPairList();
            for (int i=0; i < 120; i++)
            {
                list0.Add(new PointPair(i*0.01-0.1, 1.1-i*0.01));
            }

            LineItem myCurve = pane.AddCurve("", list0, Color.Black);

            PointPairList list = new PointPairList();
            foreach(Package pk in pm.packages.Values)
            {

                list.Add(new PointPair(pk.I, pk.A, pk.Name));
            }

            LineItem myCurve2 =  pane.AddCurve("", list, Color.Blue, 
                SymbolType.Circle);

            myCurve2.Line.IsVisible = false;

            myCurve2.Symbol.Fill.Color = Color.Blue;

            myCurve2.Symbol.Fill.Type = FillType.Solid;

            myCurve2.Symbol.Size = 30;

            zedGraphControl1.Refresh();
        }

    }
}
