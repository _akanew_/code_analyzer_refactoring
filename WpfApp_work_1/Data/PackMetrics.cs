﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_work_1.Data
{
    public class Class
    {
        public String Name { get; set; }

        public bool IsAbstract { get; set; }

        public Class(String name, bool isAbstr)
        {
            Name = name;
            IsAbstract = isAbstr;
        }
    }

    public class SingleRelation
    {
        public Class From { get; set; }
        public Class To { get; set; }
    }

    public class Package
    {
        public String Name { get; set; }

        public int N
        {
            get
            {
                return classes.Count;
            }
        }

        public int NA
        {
            get
            {
                return classes.Count(x => x.IsAbstract);
            } 
        }

        public double A
        {
            get
            {
                return (double)NA / N;
            }
        }

        public int Ce { get; set; }

        public int Ca { get; set; }

        public double I
        {
            get
            {
                return (double) Ce / (Ce + Ca);
            }
        }

        public double D
        {
            get
            {
                return Math.Abs(A + I - 1);
            }
        }

        List<Class> classes;

        public Package(string name)
        {
            Name = name;
            classes = new List<Class>();
        }

        public void AddClass(Class cls)
        {
            classes.Add(cls);
        }
    }

    public class PackMetrics
    {
        Analyze analyze;

        public Dictionary<String, Package> packages;

        public PackMetrics(Analyze _an)
        {
            analyze = _an;
            packages = new Dictionary<string, Package>();
            DoAnalyze();
        }

        void DoAnalyze()
        {
            analyze.GetInfoOfClass();
            string packName = "";
            for (int i=0; i < analyze.count_class + 1; i++)
            {
                packName = analyze.analyzeFindClass[i].Namespace;
                if (!packages.ContainsKey(packName))
                {
                    packages.Add(packName, new Package(packName));
                }
                packages[packName].AddClass(new Class(
                      analyze.analyzeFindClass[i].NameClass,
                      analyze.analyzeFindClass[i].Type == "abstract" ||
                      analyze.analyzeFindClass[i].Type == "interface"));
            }
            // Отношения
            Relation relation = analyze.relationAnalyze;

            foreach(List<int> lst in relation.Table)
            {
                for(int j=1; j < lst.Count; j++)
                {
                    if(analyze.analyzeFindClass[lst[j-1]].Namespace !=
                       analyze.analyzeFindClass[lst[j]].Namespace)
                    {
                        String pack1 = analyze.analyzeFindClass[lst[j-1]].Namespace;
                        String pack2 = analyze.analyzeFindClass[lst[j]].Namespace;
                        packages[pack1].Ca++;
                        packages[pack2].Ce++;
                    }
                }
            }

            foreach (List<int> lst in relation.TableAgregation)
            {
                for (int j = 1; j < lst.Count; j++)
                {
                    if (analyze.analyzeFindClass[lst[j - 1]].Namespace !=
                       analyze.analyzeFindClass[lst[j]].Namespace)
                    {
                        String pack1 = analyze.analyzeFindClass[lst[j - 1]].Namespace;
                        String pack2 = analyze.analyzeFindClass[lst[j]].Namespace;
                        packages[pack1].Ce++;
                        packages[pack2].Ca++;
                    }
                }
            }

            foreach (List<int> lst in relation.TableComposition)
            {
                for (int j = 1; j < lst.Count; j++)
                {
                    if (analyze.analyzeFindClass[lst[j - 1]].Namespace !=
                       analyze.analyzeFindClass[lst[j]].Namespace)
                    {
                        String pack1 = analyze.analyzeFindClass[lst[j - 1]].Namespace;
                        String pack2 = analyze.analyzeFindClass[lst[j]].Namespace;
                        packages[pack1].Ce++;
                        packages[pack2].Ca++;
                    }
                }
            }

        }

        public int PackagesCount {
            get
            {
                return packages.Count;
            }
        }

    }
}
