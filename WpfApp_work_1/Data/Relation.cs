﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_work_1.Data
{
   public class Relation
    {
        public string Name { get; }//имя задается только при создании , не меняется

        public Relation() //конcтруктор 0 который по умолчанию test
        {

        }
        public Relation(string value) //конcтруктор с именем
        {
            Name = value;
        }

        public List<List<int>> Table { get; set; }
        public List<List<int>> TableReverce { get; set; }
        public int Count_Table { get; set; } = 0; //количество связей //не используется

        public List<List<int>> TableComposition { get; set; }
        public List<List<int>> TableCompositionReverce { get; set; }
        public int Count_TableComposition { get; set; } = 0; //количество связей //не используется

        public List<List<int>> TableAgregation { get; set; }
        public List<List<int>> TableAgregationReverce { get; set; }
        public int Count_TableAgregation { get; set; } = 0; //количество связей //не используется
    }
}
