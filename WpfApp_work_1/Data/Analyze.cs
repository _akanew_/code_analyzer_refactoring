﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections; //для ArrayList
using System.Text.RegularExpressions; //для регулярных выражений
using System.IO; //для работы с файлом

namespace WpfApp_work_1.Data
{
    public class Analyze
    {
        string path_text;
        string[] text ;

        string path_field;
        string[] text_field;

        string path_2_ToMask; //не используется
        string[] text_rezervedWordClass; //не используется

        public Analyze(string path_text, string path_field, string path_2_ToMask) //конclass_braces_cntтруктор 
        {
            this.path_text= path_text;
            text = File.ReadAllLines(path_text, Encoding.Default);

            this.path_field= path_field;
            text_field = File.ReadAllLines(path_field, Encoding.Default);

            this.path_2_ToMask= path_2_ToMask;//не используется
            text_rezervedWordClass = File.ReadAllLines(path_2_ToMask, Encoding.Default);//не используется
        }

        Regex regex_class = new Regex(@"\s*class\s{1,}\w{1,}\s*|\s*interface\s{1,}\w{1,}\s*|\s*struct\s{1,}\w{1,}\s*");  //шаблон для класса пробел?_класс_пробел_имя_пробел?  ( интерфейса и структура)
        Regex regex_metod = new Regex(@"[()]");  //шаблон для поля metod имеется ()   //new Regex(@"[()]");
        public FindClass[] analyzeFindClass = new FindClass[100];//изменить на лист

        public int count_class = 0; //ипользуется в mwx.cs


        /// <summary>
        /// Считает кол-во скобок в строке
        /// </summary>
        /// <param name="codeLine">Анализируемая строка (содержащая или не содержащая фигурные скобки)</param>
        /// <returns>
        /// Если результат отрицательный значит имеется непарная закрывающая скобка
        /// Если результат положительный значит имеется непарная открывающаяся скобка
        /// Если результат 0, то строка с самозакрывающимся блоком (имеет пару скобок внутри) или не имеет скобок вовсе
        /// </returns>
        private int GetCountOfBracesInCodeLine(String codeLine) {
            Regex regex_start = new Regex(@"\{");
            Regex regex_stop = new Regex(@"\}");

            int index_open_brace = codeLine.IndexOf("{");
            int index_close_brace = codeLine.IndexOf("}");
            int index_open_quote = codeLine.IndexOf('"');
            int index_close_quote = codeLine.LastIndexOf('"');

            int cnt_open_braces = regex_start.Matches(codeLine).Count;
            int cnt_close_braces = regex_stop.Matches(codeLine).Count;

            // Исключаем фигурные скобки являющиеся текстом
            if ((index_open_brace > index_open_quote) & (index_open_brace < index_close_quote))
            {
                cnt_open_braces = cnt_open_braces - 1;
            }
            if ((index_close_brace > index_open_quote) & (index_close_brace < index_close_quote))
            {
                cnt_close_braces = cnt_close_braces - 1;
            }

            //
            int count_braces_in_line = cnt_open_braces - cnt_close_braces;

            return count_braces_in_line;
        }

        /// <summary>
        /// Определяет факт закрытия области видимости (тела между фигурными скобками)
        /// </summary>
        /// <param name="commonBraceCnt">Кол-во фигурных скобок для некоторого типа анализируемого блока</param>
        /// <param name="currentBraceCnt">Кол-во фигурных скобок найденных на текущем шаге (в некоторой анализируемой стоке)</param>
        /// <returns>
        /// true - Блок закрыт
        /// false - Кол-во фигурных скобок не совпадает, блок по-прежнему открыт
        /// </returns>
        private bool isCloseBrace(int commonBraceCnt, int currentBraceCnt)
        {
            int prevCommonBraceCnt = commonBraceCnt;
            commonBraceCnt = commonBraceCnt + currentBraceCnt;

            //Скобки закрыты если была открыта и в сумме дали 0
            if (prevCommonBraceCnt > 0 && commonBraceCnt == 0)
                return true;
            else return false;
        }

        /// <summary>
        /// Найден метод. Длинна метода для текущей функции неизвестна
        /// на данном этапе мы формируем первоначальное описание метода относительно определяющего его класса
        /// </summary>
        /// <param name="codeLine">Текущая аналзируемая строка</param>
        /// <param name="codePosition">Индекс строки в цикле (анализируемом массиве строк)</param>
        /// <returns>
        /// true - Метод найден. Блок открыт.
        /// false - В текущей строке не содержится информации о методе
        /// </returns>
        private bool isMethodBegin(String codeLine, int codePosition)
        {
            if ((regex_metod.IsMatch(codeLine) == true) & (codeLine.IndexOf(";") == -1)) //шаблон найден если есть скобки и нету ;
            {
                int method_Id = analyzeFindClass[count_class].Metod_Count;
                analyzeFindClass[count_class].Metod[method_Id] = new ArrayList();
                analyzeFindClass[count_class].Metod[method_Id].Add(codeLine);
                analyzeFindClass[count_class].Metod[method_Id].Add(codePosition);
                analyzeFindClass[count_class].Metod_Count++;

                return true;
            }
            else return false;
        }

        /// <summary>
        /// Найдено пространство имён. Длинна namespace-а для текущей функции неизвестна
        /// </summary>
        /// <param name="codeLine">Текущая аналзируемая строка</param>
        /// <returns>
        /// Имя пространства имен или null в случае его отсутствия
        /// </returns>
        private String getNamespaceIfExist(String codeLine)
        {
            Regex regex_namespace = new Regex(@"^\s*namespace\s{1,}");
            Regex regex_name_of_namespace = new Regex(@"\s*\w+\.?\w*\s*"); ;// слово+ точка? слово*? 

            if (regex_namespace.IsMatch(codeLine) == true)
            {
                MatchCollection matchCollection = regex_name_of_namespace.Matches(codeLine);
                String name_of_namespace = matchCollection[1].ToString();  //второе слово в строке 
                return name_of_namespace;
            }
            else return null;
        }

        /// <summary>
        /// Возвращает тип базовой структуры: abstract, interface, struct
        /// </summary>
        /// <param name="codeLine">Текущая аналзируемая строка</param>
        /// <returns>
        /// Имя структуры данных (abstract, interface, struct) или null в случае его отсутствия
        /// </returns>
        private String getTypeClassIfExist(String CodeLine)
        {
            Regex regex_abstract = new Regex(@"\s*abstract\s{1,}");//шаблон слова пробел?_абстракт_пробел?
            Regex regex_interface = new Regex(@"\s*interface\s{1,}");
            Regex regex_struct = new Regex(@"\s*struct\s{1,}");

            if (regex_abstract.IsMatch(CodeLine) == true)
                return "abstract";
            else if (regex_interface.IsMatch(CodeLine) == true)
                return "interface";
            else if (regex_struct.IsMatch(CodeLine) == true)
                return "struct";
            else return null;
        }

        /// <summary>
        /// Возвращает имя класса
        /// </summary>
        /// <param name="codeLine">Текущая аналзируемая строка</param>
        /// <returns>
        /// Имя класса или null в случае его отсутствия
        /// </returns>
        private String getClassNameFromCodeLine(String CodeLine)
        {
            Regex regex_value = new Regex(@"\w{1,}");//шаблон слова
            Regex regex_c_i = new Regex(@"class|interface|struct");//шаблон слова
            int count_word = 0;

            MatchCollection matchCollection = regex_value.Matches(CodeLine);//разбиваем текст на слова

            //Имя класса/интерфеса/структуры лежит сразу после ключевого слова
            for (int cc = 0; cc < matchCollection.Count; cc++)
            {
                if (regex_c_i.IsMatch(matchCollection[cc].ToString()) == true)
                    count_word = cc + 1;
            }

            if (count_word > 0)
                return matchCollection[count_word].ToString();
            else return null;
        }



        public void GetInfoOfClass()
        {
            Regex regex_nofield = new Regex(@"[()]");

            Regex regex_noMetod = new Regex(@"[();]");
            Regex regex_word = new Regex(@"\w+");
            Regex regex_region = new Regex(@"\W*#region\W*|\W*#endregion\W*");

            //для namespace
            string str_name_of_namespace=null;
            bool l_namespace = false;
            int namespace_braces_cnt = 0;

            bool l_class = false;
            bool l_notMethod = false;
            bool l_metod = false;
            bool l_inside_metod = false; //не используется

            #region анализ исходного текста , поиск namespace, классов , методов, полей

            int class_braces_cnt = 0; //счетчик скобок { и } для класса
            int method_braces_cnt = 0; //счетчик скобок { и } для медота
            int not_method_braces_cnt = 0;//счетчик скобок { и } для неМедота
            int b = 0;
            int b1 = 0;// переменная для начала позиции перебора в тексте шаблона
            count_class = -1;// для выравнивания индекса первого найденного класса -> отчет с 0
            

            bool? step_namespace = null;
            bool? step_class = null;
            bool? step_noMetod = null;


            Regex regex_start = new Regex(@"\{");
            Regex regex_stop = new Regex(@"\}");
            Regex regex_quote = new Regex("\"");
            int tt = 0;
            Regex regex_class2 = new Regex(@"\s*class\s{1,}\w{1,}\s*|\s*interface\s{1,}\w{1,}\s*|\s*struct\s{1,}\w{1,}\s*");  //шаблон для класса пробел?_класс_пробел_имя_пробел?  ( интерфейса и структура)

            //перебор с начала до конца текста
            for (int i = 0; i < text.Length; i++)
            {
                if (regex_class2.IsMatch(text[i]) ==true)
                    tt = 0;
                else
                    tt = 1;

                //Определяем кол-во фигурных скобок в строке
                int count_braces_in_line = GetCountOfBracesInCodeLine(text[i]);

                #region //Проверка namespace
                //Проверяем наличие namespace
                if (!l_namespace) {
                    if ((str_name_of_namespace = getNamespaceIfExist(text[i])) != null)
                        l_namespace = true;
                }
                else //когда найден namespace считаем скобки 
                {
                    if (!l_class)
                    {
                        //Считаем namespace закрытым при наличии парной скобки
                        //если namespace_braces_cnt + count_braces_in_line = 0 - скобки зарыты
                        if (isCloseBrace(namespace_braces_cnt, count_braces_in_line))
                            l_namespace = false; //namespace закончен

                        namespace_braces_cnt = namespace_braces_cnt + count_braces_in_line;
                    }
                }
                #endregion

                #region// поиск шаблон класса  
                if (!l_class)
                {
                    if (regex_class.IsMatch(text[i]) == true) {
                        l_class = true;
                        count_class++;

                        //инициализируем новый класс
                        analyzeFindClass[count_class] = new FindClass();
                        analyzeFindClass[count_class].Field = new ArrayList[100];
                        analyzeFindClass[count_class].Metod = new ArrayList[100];
                        analyzeFindClass[count_class].Namespace = str_name_of_namespace;
                        analyzeFindClass[count_class].Type = getTypeClassIfExist(text[i]);//Тип структуры данных
                        analyzeFindClass[count_class].StartPosition = i;
                        analyzeFindClass[count_class].NameClass = getClassNameFromCodeLine(text[i]);
                        analyzeFindClass[count_class].Number = count_class; //номер класса 
                    }
                }
                else //когда найден класс считаем скобки (начало и конец класса) 
                {
                    //Считаем класс закрытым при наличии парной скобки
                    //если class_braces_cnt + count_braces_in_line = 0 - скобки зарыты
                    if (isCloseBrace(class_braces_cnt, count_braces_in_line))
                    {
                        l_class = false; //найден конец для class (закрывающая фигурная скобка)
                        analyzeFindClass[count_class].StopPosition = i;
                    }

                    class_braces_cnt = class_braces_cnt + count_braces_in_line;
                }
                #endregion

                #region// поиск шаблон поля (field)
                //шаблон поля найден только если найден класс но не вошли в метод 
                if ((l_class == true) & (l_metod == false) & (l_notMethod == false)) {
                    if (text[i].IndexOf(";") != -1)
                    {
                        for (int i2 = 0; i2 < text_field.Length; i2++)
                        {
                            Regex regex_field = new Regex(@"\W*" + text_field[i2] + @"\W+");
                            if ((regex_field.IsMatch(text[i]) == true))
                            {
                                int field_id = analyzeFindClass[count_class].Field_Count;
                                analyzeFindClass[count_class].Field[field_id] = new ArrayList();
                                analyzeFindClass[count_class].Field[field_id].Add(text[i]);
                                analyzeFindClass[count_class].Field[field_id].Add(i);

                                analyzeFindClass[count_class].Field_Count++;
                                break;
                            }
                        }
                    }
                }

                    /*if (text[i].IndexOf(";") != -1){
                    for (int i2 = 0; i2 < text_field.Length; i2++)
                    {
                        //пробуем дрогой подход Regex regex_field = new Regex(@"\W*" + text_field[i2] + @"\W+");//шаблон поля поиск из файла field по зарезервиравнному слову
                        Regex regex_field = new Regex(@"(\;\W*)$");//шаблон поля поиск по точке c запятой ";"
                        bool test = regex_field.IsMatch(text[i]);
                        int testint = text[i].IndexOf(";");

                        if ((regex_field.IsMatch(text[i]) == true) & (text[i].IndexOf(";") != -1))            //  ( (a) & (b==false) & ( (class_braces_cnt) | (d) ))           // нашли поле если есть зарезервированые слова и есть ;  // & (regex_nofield.IsMatch(text[i]) != true))
                        {
                            if ((l_class == true) & (l_metod == false) & (l_notMethod == false))  //шаблон полня найден только если найден класс но не вошли в метод 
                            {

                                i2 = text_field.Length;// 1 совпадение найдено выходим и цикла

                                //записываем данные в класс FindClass
                                //при использовании ArrayList
                                int field_id = analyzeFindClass[count_class].Field_Count;
                                analyzeFindClass[count_class].Field[field_id] = new ArrayList();
                                analyzeFindClass[count_class].Field[field_id].Add(text[i]);//записываем в класс строку с полем //findClass[count_class].Field[count_field][0];
                                analyzeFindClass[count_class].Field[field_id].Add(i);//записываем в класс номер строки   startposition   //findClass[count_class].Field[count_field][1];

                                ////при использовании  List
                                analyzeFindClass[count_class].Field_Count++;//записываем в класс количетво полей
                                                                            //   label_to_box.Text += Environment.NewLine + "Количество полей=" + count_field + "строка=" + i;
                            }
                        }
                    }
                }*/
                #endregion

                #region// поиск шаблон метода 
                if ((l_class == true) & (l_notMethod == false))
                {
                    // Содержимое - метод или его тело? 
                    // l_metod=true - да, l_metod=false - нет
                    if (!l_metod)
                    { // аналогично проверке (l_metod == false && method_braces_cnt == 0)
                        l_metod = isMethodBegin(text[i], i);
                    }
                    else //когда найден metod считаем скобки 
                    {
                        //Считаем метод закрытым при наличии парной скобки
                        //если method_braces_cnt + count_braces_in_line = 0 - скобки зарыты
                        if (isCloseBrace(method_braces_cnt, count_braces_in_line))
                        {
                            l_metod = false; //найден конец для metod (закрывающая фигурная скобка)
                            int method_Id = analyzeFindClass[count_class].Metod_Count - 1;
                            analyzeFindClass[count_class].Metod[method_Id].Add(i);
                        }

                        method_braces_cnt = method_braces_cnt + count_braces_in_line;
                    }
                }
                #endregion

                #region// поиск шаблона НЕ МЕТОДА  //поиск ведется вне метода
                //Исключаем все варианты являющися полями, методами класса, пространством имен и самим классом (особое внимание regex_noMetod - он верный!!!)
                if ((regex_word.IsMatch(text[i]) == true) & (regex_noMetod.IsMatch(text[i]) == false) & (regex_class.IsMatch(text[i]) == false) & regex_region.IsMatch(text[i]) == false) //шаблон есть слово , нет [];  , нет региона
                {

                    if ( (l_class == true) & (l_metod == false))
                    {
                            int field_id = analyzeFindClass[count_class].Field_Count;
                            analyzeFindClass[count_class].Field[field_id] = new ArrayList();
                            analyzeFindClass[count_class].Field[field_id].Add(text[i]);
                            analyzeFindClass[count_class].Field[field_id].Add(i);

                            analyzeFindClass[count_class].Field_Count++;
                    }
                }

                #region //считаем {} начало и конец no field поиск по сумме {}
                if (l_notMethod == true) //Предположительно лишний, т.к. field не имеет скобок, если он не структура или перечисление
                {
                    //Считаем класс закрытым при наличии парной скобки
                    //если not_method_braces_cnt + count_braces_in_line = 0 - скобки зарыты
                    if (isCloseBrace(not_method_braces_cnt, count_braces_in_line))
                    {
                        l_notMethod = false; //not_method_braces_cnt закончен
                        int field_id = analyzeFindClass[count_class].Field_Count - 1;
                        analyzeFindClass[count_class].Field[field_id].Add(i);//записываем в класс номер строки
                    }

                    not_method_braces_cnt = not_method_braces_cnt + count_braces_in_line;
                }
                #endregion

                #endregion

            }
            #endregion

            // return analyzeFindClass;
            FindDependence();
            FindAssociation();
        }

        public Relation relationAnalyze = new Relation();

        public void FindDependence()
        {      
            //считываем анализируемый текст в массив Console.WriteLine("******считываем анализируемый текст в массив********");
            string[] text2 = File.ReadAllLines(path_text, Encoding.Default);

            //считываем шаблон маску в массив          
            string[] textMask2 = File.ReadAllLines(path_2_ToMask, Encoding.Default);

            //считываем шаблон field в массив
            string[] text_field2 = File.ReadAllLines(path_field, Encoding.Default);

           // Regex regex_class = new Regex(@"\s*class\s{1,}\w{1,}\s*");  //шаблон для класса пробел?_класс_пробел_имя_пробел?  (добавить для интерфейса и т.д?????????воможно нет)
                                                                        //*****************провести рефкторинг ************************************************************************************************
            // label_to_box.Clear();

            #region //поиск зависимостей обычная зависимость relation1 
           // Relation relationAnalyze = new Relation();
            relationAnalyze.Table = new List<List<int>>();
            relationAnalyze.Count_Table = new int(); //

            List<int> relationInClassAnalyze = new List<int>(); //промежуточный лист

            for (int i = 0; i < text2.Length; i++)
            {
                if ((regex_class.IsMatch(text2[i])) & (text2[i].IndexOf(":") != -1))  // если в строке найден шаблон класса и двоеточие 
                {
                    //делим строку на две части , до  и  после двоеточия
                    string[] words = text2[i].Split(new char[] { ':' });
                    words[0] = String.Concat(words[0], " ");//если сразу после имени класса было:
                    words[1] = String.Concat(" ", words[1]);//если после : было имя класс
                    words[1] = String.Concat(words[1], " ");//если вконце после класса не было пробела

                    //(поиск первого класса)  имя класса находится до двоеточия < :
                    for (int r = 0; r <= count_class; r++) // поиск в массиве найденных классов
                    {
                       
                        Regex regex_nameClass = new Regex(  String.Format(@"\W+{0}\W+", analyzeFindClass[r].NameClass)  );
                        bool find = regex_nameClass.IsMatch(words[0]);

                        if (find == true)  //если имя класса раньше двоеточия 
                        {
                            relationInClassAnalyze.Add(analyzeFindClass[r].Number);
                        }
                    }

                    //(поиск класса фром наследника) имя класса > : после двоеточия
                    for (int r = 0; r <= count_class; r++)
                    {

                        Regex regex_nameClass = new Regex(String.Format(@"\W+{0}\W+", analyzeFindClass[r].NameClass));
                        bool find = regex_nameClass.IsMatch(words[1]);
                        if (find==true) //совпадение класса во второй части найдено
                        {
                            relationInClassAnalyze.Add(analyzeFindClass[r].Number);
                            relationAnalyze.Count_Table++; //подсчет количества связей
                          //  label_to_box.Text += Environment.NewLine + "Найдены классы от которых наследуется=" + findClass[r].Number;
                        }
                    }

                    relationAnalyze.Table.Add(relationInClassAnalyze);
                }

                //  relationInClass.Clear(); 
                /// АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ АХТУНГ   затирает все даже в relation1.Table
                relationInClassAnalyze = new List<int>();
            }

            //таблица зависимостей reverce нужна для отрисовки
            relationAnalyze.TableReverce = new List<List<int>>();

            for (int a = 0; a <= count_class; a++)
            {
                relationInClassAnalyze = new List<int>();
                relationInClassAnalyze.Add(a);
                relationAnalyze.TableReverce.Add(relationInClassAnalyze);

                for (int i = 0; i < relationAnalyze.Table.Count; i++)
                    for (int j = 1; j < relationAnalyze.Table[i].Count; j++)
                    {
                        if (relationAnalyze.Table[i][j] == a)
                        {
                            relationAnalyze.TableReverce[a].Add(relationAnalyze.Table[i][0]);
                        }
                    }
            }

            #endregion
        }

        public void FindAssociation()
        {
            relationAnalyze.TableComposition = new List<List<int>>();
            relationAnalyze.TableAgregation = new List<List<int>>();
            List<int> temp_relationInClassAnalyze = new List<int>();
            List<int> temp_relationInClassAnalyzeAgregation = new List<int>();
            string temp_text ;
            int del_i = 0;
            int del_j = 0;
            bool com = false;
            bool agr = false;

            int count_TableComposition = 0; //количество связей копозиция
            int count_TableAgregation = 0;
            for (int i = 0; i <= count_class; i++)//перебор классов i
            {
                temp_relationInClassAnalyze = new List<int>();
                temp_relationInClassAnalyzeAgregation = new List<int>();
                // temp_relationInClassAnalyze.Add(i);

                com = false; //первый composiyion элемент = не найден
                agr = false;

                count_TableComposition = 0;
                count_TableAgregation = 0;
                for (int j = 0; j <= count_class; j++)//перебор зависимых классов j в классе i
                {

                    Regex regex_Composition = new Regex(@"=\s*new\s+(\w*\W+)?" + analyzeFindClass[j].NameClass + @"\W+");//шаблон композиции  учтен список классов <класс>

                    Regex regex_Agregation = new Regex(@"\W+" + analyzeFindClass[j].NameClass + @"\W+\w+");  //шаблон агрегации    пробел имя_класса пробел слово

                    #region  //определение композиции
                    for (int iStart = analyzeFindClass[i].StartPosition; iStart <= analyzeFindClass[i].StopPosition; iStart++)
                    {                    
                        temp_text = text[iStart]; //отладка
                        if ( (regex_Composition.IsMatch(text[iStart])==true) & (i!=j)  )  //если найден шаблон композиции в текущей строке  каждую строку проверяем на "= new ИМЯ_КЛАССА"  ИМЯКЛАССА перебор во всех классах
                        {
                            if (com == false)//находим класс и зависимость
                            {
                                 del_i = i + 1;//не используется
                                del_j = j + 1;//не используется
                                temp_relationInClassAnalyze.Add(i);  //добавили 1й элемент ИМЯКЛАССА в таблицу
                                com = true;//первый элемент = найден
                            }
                            //добавляем в таблицу   TableComposition[0]= analyzeFindClass[i].Number
                            temp_relationInClassAnalyze.Add( j);//добавили зависимость
                            if (j > count_TableComposition)
                                count_TableComposition =j;//  ????????????                                               
                        }                                   
                    }

                    #endregion

                    #region  //определение агрегации

                    for (int method_braces_cnt = 0; method_braces_cnt < analyzeFindClass[i].Metod_Count; method_braces_cnt++)
                    {
                        string str_metod = analyzeFindClass[i].Metod[method_braces_cnt][0].ToString();                  
                        bool t1 = regex_Agregation.IsMatch(str_metod);
                        bool t2 = i != j;
                        bool t3 = regex_metod.IsMatch(str_metod);
                        bool t4 = str_metod.IndexOf(";") == -1;

                        if ((regex_Agregation.IsMatch(str_metod) == true) & (i != j) & (regex_metod.IsMatch(str_metod) == true) & (str_metod.IndexOf(";") == -1))  //если найден шаблон агрегации и нету композиции в текущей строке с методом  //нету композиции убрали  & (regex_Composition.IsMatch(text[iStart]) == false)
                        {

                            if (agr == false)//находим класс и зависимость
                            {
                                del_i = i + 1;//не используется
                                del_j = j + 1;//не используется
                                temp_relationInClassAnalyzeAgregation.Add(i);  //добавили 1й элемент ИМЯКЛАССА в таблицу
                                agr = true;//первый элемент = найден
                            }
                            //добавляем в таблицу   TableCompositionAgregation[0]= analyzeFindClass[i].Number
                            temp_relationInClassAnalyzeAgregation.Add(j);//добавили зависимость
                            if (j > count_TableAgregation)
                                count_TableAgregation = j;
                        }
                        // }                
                    }

                    #endregion

                }

                if (temp_relationInClassAnalyze.Count > 1)//если больше 1 найденного элемента
                {
                    relationAnalyze.TableComposition.Add(temp_relationInClassAnalyze);
                    relationAnalyze.Count_TableComposition = count_TableComposition;
                }

                if (temp_relationInClassAnalyzeAgregation.Count > 1)//если больше 1 найденного элемента
                {
                    relationAnalyze.TableAgregation.Add(temp_relationInClassAnalyzeAgregation);
                    relationAnalyze.Count_TableAgregation = count_TableAgregation;
                }
            }

            //таблица зависимостей reverce 
            relationAnalyze.TableCompositionReverce = new List<List<int>>();
            for (int a = 0; a <= count_class; a++)
            {
                temp_relationInClassAnalyze = new List<int>();
                temp_relationInClassAnalyze.Add(a);
                relationAnalyze.TableCompositionReverce.Add(temp_relationInClassAnalyze);

                for (int i = 0; i < relationAnalyze.TableComposition.Count; i++)
                    for (int j = 1; j < relationAnalyze.TableComposition[i].Count; j++)
                    {
                        if (relationAnalyze.TableComposition[i][j] == a)
                        {
                            relationAnalyze.TableCompositionReverce[a].Add(relationAnalyze.TableComposition[i][0]);                         
                        }
                    }
            }

            //таблица зависимостей reverce 
            relationAnalyze.TableAgregationReverce = new List<List<int>>();
            for (int a = 0; a <= count_class; a++)
            {
                temp_relationInClassAnalyzeAgregation = new List<int>();
                temp_relationInClassAnalyzeAgregation.Add(a);
                relationAnalyze.TableAgregationReverce.Add(temp_relationInClassAnalyzeAgregation);

                for (int i = 0; i < relationAnalyze.TableAgregation.Count; i++)
                    for (int j = 1; j < relationAnalyze.TableAgregation[i].Count; j++)
                    {
                        if (relationAnalyze.TableAgregation[i][j] == a)
                        {
                            relationAnalyze.TableAgregationReverce[a].Add(relationAnalyze.TableAgregation[i][0]);

                        }
                    }
            }
        }   
    }
}