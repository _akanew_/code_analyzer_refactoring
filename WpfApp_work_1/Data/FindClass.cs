﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Collections; //для ArrayList
using System.Text.RegularExpressions; //для регулярных выражений

namespace WpfApp_work_1.Data
{
    public class FindClass
    {

        #region //Цвет линий test

        public string ColorName = "Red";
        public void ColorLine(string ColorInput)
        {
            // SolidColorBrush ColorName2 = (SolidColorBrush)new BrushConverter().ConvertFromString(this.ColorInput);

            this.ColorName = ColorInput;
            // return BrushConverter().ConvertFromString(ColorInput);
            // 
        }
        #endregion

        public int Number { get; set; }
        public int StartPosition { get; set; }
        public int StopPosition { get; set; }
        public string NameClass { get; set; }
        public string Type { get; set; } //тип класса абстрактный или интерфейс
        public string Namespace { get; set; } // namespace

        public int Class_Count { get; set; }//количесто классов //?????????

        //public List<string> Field { get; set; } //строки с полями field
        public ArrayList[] Field { get; set; }//строки с полями field
        public int Field_Count { get; set; }

        //public List<string> Metod { get; set; } //строки с методами
        public ArrayList[] Metod { get; set; }//строки с методами
        public int Metod_Count { get; set; }   
    }
}
